<?php
/**
 * Digi Users
 *
 * @package   Digi Users Document
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * User Meta box class.
 *
 * @package   Digi Users Document
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 */
class DigiUsersDocument_Meta_Box {

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug;

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the meta boxes
	 *
	 * @since     1.0.0
	 */
	public function __construct() {

		$digi_users_document = DigiUsersDocument::get_instance();
		$this->plugin_slug = $digi_users_document->get_plugin_slug();

		// Setup the meta boxes for the user
		add_action( 'add_meta_boxes', array( $this, 'add_digi_users_document_meta_box' ) );

		// Setup the function responsible for saving
		add_action( 'save_post', array( $this, 'save_digi_users_document_details' ) );
		// Setup the function responsible for deliting
		// add_action( 'delete_post ', array( $this, 'delete_digi_users_document_details' ) );
		
		
	}

	/**
	 * Registers the meta box for the post editor.
	 *
	 * @since      1.0.0
	 */
	public function add_digi_users_document_meta_box() {
		
		add_meta_box(
			'digi_users_document_file',
			__( 'File', $this->plugin_slug ),
			array( $this, 'display_digi_users_document_file_meta_box' ),
			'digi_users_document',
			'normal',
			'default'
		);
		
		add_meta_box(
			'digi_users_document_cat',
			__( 'Taxonomies', $this->plugin_slug ),
			array( $this, 'display_digi_users_document_cat_meta_box' ),
			'digi_users_document',
			'normal',
			'default'
		);
		
		add_meta_box(
			'digi_users_document_user',
			__( 'Users', $this->plugin_slug ),
			array( $this, 'display_digi_users_document_user_meta_box' ),
			'digi_users_document',
			'normal',
			'default'
		);

	}

	/**
	 * Displays the meta box 
	 *
	 * @since      1.0.0
	 */
	public function display_digi_users_document_file_meta_box( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), 'digi-users-document-nonce' );
		$digi_users_document_stored_meta = get_post_meta( $post->ID );

		include_once( plugin_dir_path( __FILE__ ) . 'views/display-digi-users-document-file-meta-box.php' );

	}
	public function display_digi_users_document_cat_meta_box( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), 'digi-users-document-nonce' );
		$digi_users_document_stored_meta = get_post_meta( $post->ID );

		include_once( plugin_dir_path( __FILE__ ) . 'views/display-digi-users-document-cat-meta-box.php' );

	}
	public function display_digi_users_document_user_meta_box( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), 'digi-users-document-nonce' );
		$digi_users_document_stored_meta = get_post_meta( $post->ID );

		include_once( plugin_dir_path( __FILE__ ) . 'views/display-digi-users-document-user-meta-box.php' );

	}

	/**
	 * When the post is saved or updated, generates a short URL to the existing post.
	 *
	 * @param    int     $post_id    The ID of the post being save
	 * @since    1.0.0
	 */
	public function save_digi_users_document_details( $post_id ) {
		
		

		if ( $this->user_can_save( $post_id, 'digi-users-document-nonce' ) ) {
			

			 // // Check, validate and save checkboxes
 // 			$checkboxes = array(
 // 				'fp5-autoplay',
 // 				'fp5-loop',
 // 				'fp5-fixed-controls',
 // 				'fp5-aspect-ratio',
 // 				'fp5-fixed-width',
 // 				'fp5-no-background',
 // 				'fp5-aside-time',
 // 				'fp5-no-hover',
 // 				'fp5-no-mute',
 // 				'fp5-no-time',
 // 				'fp5-no-volume',
 // 				'fp5-no-embed',
 // 				'fp5-play-button'
 // 			);
 // 
 // 			foreach ( $checkboxes as $checkbox ) {
 // 				if( isset( $_POST[ $checkbox ] ) ) {
 // 					update_post_meta(
 // 						$post_id,
 // 						$checkbox,
 // 						'true'
 // 					);
 // 				} else {
 // 					update_post_meta(
 // 						$post_id,
 // 						$checkbox,
 // 						''
 // 					);
 // 				}
 // 			}
 // 
 // 			// Check, validate and save keys
 // 			$keys = array(
 // 				'fp5-select-skin',
 // 				'fp5-preload',
 // 				'fp5-coloring',
 // 				'fp5-ad-type'
 // 			);
 // 
 // 			foreach ( $keys as $key ) {
 // 				if( isset( $_POST[ $key ] ) ) {
 // 					update_post_meta(
 // 						$post_id,
 // 						$key,
 // 						sanitize_key( $_POST[ $key ] )
 // 					);
 // 				}
 // 			}
 // 
 // 			// Check, validate and save urls
 // 			$urls = array(
 // 				'fp5-splash-image',
 // 				'fp5-mp4-video',
 // 				'fp5-webm-video',
 // 				'fp5-ogg-video',
 // 				'fp5-hls-video',
 // 				'fp5-vtt-subtitles'
 // 			);
 // 
 // 			foreach ( $urls as $url ) {
 // 				if( isset( $_POST[ $url ] ) ) {
 // 					update_post_meta(
 // 						$post_id,
 // 						$url,
 // 						esc_url_raw( $_POST[ $url ] )
 // 					);
 // 				}
 // 			}
 // 
 // 			// Check, validate and save numbers
 // 			$numbers = array(
 // 				'fp5-max-width',
 // 				'fp5-width',
 // 				'fp5-height',
 // 				'fp5-user-id',
 // 				'fp5-video-id',
 // 				'fp5-ads-time'
 // 			);
 // 
 // 			foreach ( $numbers as $number ) {
 // 				if( isset( $_POST[ $number ] ) ) {
 // 					update_post_meta(
 // 						$post_id,
 // 						$number,
 // 						absint( $_POST[ $number ] )
 // 					);
 // 				}
 // 			}

			//Check, validate and save text fields
			$text_fields = array(
				'digi_users_document_file',
				'digi_users_document_taxonomies',
				'digi_users_document_user'
			);

			foreach ( $text_fields as $text_field ) {
				if( isset( $_POST[ $text_field ] ) ) {
					if(is_array($_POST[ $text_field ])){
						$_POST[ $text_field ] = serialize($_POST[ $text_field ]);
					}
					update_post_meta(
						$post_id,
						$text_field,
						sanitize_text_field( $_POST[ $text_field ] )
					);
				}else{
					update_post_meta(
						$post_id,
						$text_field,
						''
					);
				}
			}
			
			$save_taxonomies = array();
			$taxonomies = get_object_taxonomies('digi_user', 'objects');
			foreach($taxonomies as $taxonomy):
				if($_POST[$taxonomy->name]):
					$save_taxonomies[$taxonomy->name] = $_POST[$taxonomy->name];
				endif;
			endforeach;
			
			if(count($save_taxonomies)){
			update_post_meta(
				$post_id,
				'digi_users_document_taxonomies',
				sanitize_text_field( serialize($save_taxonomies) )
			);
			}else{
				update_post_meta(
					$post_id,
					'digi_users_document_taxonomies',
					''
				);
			}
			

			// echo "<pre>";
// 			print_r($_POST);
// 			echo "</pre>";
// 			exit;

		}

	}
	
	public function delete_digi_users_document_details($post_id){
		if ( $this->user_can_save( $post_id, 'digi-users-document-nonce' ) ) {
			$text_fields = array(
				'digi_users_document_file',
				'digi_users_document_taxonomies',
				'digi_users_document_user'
			);

			foreach ( $text_fields as $text_field ) {
				if( isset( $_POST[ $text_field ] ) ) {
					delete_post_meta(
						$post_id,
						$text_field
					);
				}
			}
		}
	}

	/**
	 * Determines whether or not the current user has the ability to save meta data associated with this post.
	 *
	 * @param    int     $post_id    The ID of the post being save
	 * @param    string  $nonce      The nonce identifier associated with the value being saved
	 * @return   bool                Whether or not the user has the ability to save this post.
	 * @since    1.0.0
	 */
	private function user_can_save( $post_id, $nonce ) {

		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ $nonce ] ) && wp_verify_nonce( $_POST[ $nonce ], plugin_basename( __FILE__ ) ) ) ? true : false;

		// Return true if the user is able to save; otherwise, false.
		return ! ( $is_autosave || $is_revision) && $is_valid_nonce;

	}

}
