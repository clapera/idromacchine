<?php
/**
* Digi Users Document
*
* @package   Digi Users Document
* @author    Carlo La Pera <carlo.lapera@diginess.it>
* @license   GPL-2.0+
* @link      http://diginess.com/
* @copyright 2014 Diginess s.r.l.
*/
$digi_users_document_user = unserialize(unserialize($digi_users_document_stored_meta['digi_users_document_user'][0]));
?>
<div class="digi-users-document-user-meta-box">
		<table>
			
			<!-- cat -->	
			<tr>
				<td>
					<?php 
					$post_type = 'digi_user';
						$args = array('post_type' => $post_type, 'posts_per_page' => -1);
						$users = new WP_Query($args);
						?>
						<?php if($users->have_posts()): ?>
							<select name="digi_users_document_user[]" class="chosen-select" tabindex="-1" multiple="" data-placeholder="<?php _e('Choose users', $this->plugin_slug); ?>">
			              	  <option value=""></option>
							<?php while($users->have_posts()): $users->the_post(); $email = get_post_meta(get_the_ID(),'digi-user-email',true); ?>
								<?php if(in_array(get_the_ID(),$digi_users_document_user)): ?>
	  			              	  <option value="<?php the_ID(); ?>" selected ><?php echo $email; ?></option>
								  <?php else: ?>
			  			              <option value="<?php the_ID(); ?>"><?php echo $email; ?></option>
								  <?php endif; ?>
							<?php endwhile; ?>
			            	</select>
						<?php else: ?>
							<p><?php _e('No Users Found', $this->plugin_slug); ?></p>
						<?php endif; ?>
					<?php wp_reset_query(); ?>
				</td>
			</tr>
				
		</table>
</div>

  <script type="text/javascript">
    var config = {
      '.chosen-select'           : {},
      '.chosen-select-deselect'  : {allow_single_deselect:true},
      '.chosen-select-no-single' : {disable_search_threshold:10},
      '.chosen-select-no-results': {no_results_text:'Oops, nothing found!'},
      '.chosen-select-width'     : {width:"95%"}
    }
    for (var selector in config) {
      jQuery(selector).chosen(config[selector]);
    }
  </script>