<?php
/**
* Digi Users Document
*
* @package   Digi Users Document
* @author    Carlo La Pera <carlo.lapera@diginess.it>
* @license   GPL-2.0+
* @link      http://diginess.com/
* @copyright 2014 Diginess s.r.l.
*/
$digi_users_document_taxonomies = unserialize(unserialize($digi_users_document_stored_meta['digi_users_document_taxonomies'][0]));
?>
<div class="digi-users-document-cat-meta-box">
		<table>
			
			<!-- cat -->	
			<tr>
				<td>
					<?php 
						$taxonomies = get_object_taxonomies('digi_user', 'objects');
						foreach($taxonomies as $taxonomy):
							$terms = get_terms(array($taxonomy->name));
							if(count($terms)):
								echo "<h4>".$taxonomy->labels->name."</h4>";
								foreach($terms as $term):
									if(isset($digi_users_document_taxonomies[$taxonomy->name]) && in_array($term->term_id,$digi_users_document_taxonomies[$taxonomy->name])){
										echo '<input type="checkbox" id="'.$taxonomy->name.'_'.$term->term_id.'" name="'.$taxonomy->name.'[]" value="'.$term->term_id.'" checked/>';
									}else{
										echo '<input type="checkbox" id="'.$taxonomy->name.'_'.$term->term_id.'" name="'.$taxonomy->name.'[]" value="'.$term->term_id.'"/>';
									}
									echo '<label for="'.$taxonomy->name.'_'.$term->term_id.'">'.$term->name.'</label> ';
								endforeach;
							endif;
						endforeach;
					?>
				</td>
			</tr>
				
		</table>
</div>