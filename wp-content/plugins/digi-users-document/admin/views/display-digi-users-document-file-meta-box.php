<?php
/**
* Digi Users Document
*
* @package   Digi Users Document
* @author    Carlo La Pera <carlo.lapera@diginess.it>
* @license   GPL-2.0+
* @link      http://diginess.com/
* @copyright 2014 Diginess s.r.l.
*/
?>
<div class="digi-users-document-meta-box">
		<table>
			
			<!-- File -->	
			<tr>
				<td>
					<div class="inner">
						<div data-library="all" data-preview_size="thumbnail" class="digi-users-document-media-uploader clearfix active">
							<input type="hidden" value="<?php if($digi_users_document_stored_meta['digi_users_document_file'][0]) : echo $digi_users_document_stored_meta['digi_users_document_file'][0]; endif; ?>" name="digi_users_document_file" class="digi-users-document-media-value">
							<div class="has-image" <?php if($digi_users_document_stored_meta['digi_users_document_file'][0]) : ?> style="display:block" <?php endif; ?>>
								<div class="hover">
									<ul class="bl">
										<li><a href="#" class="digi-users-document-button-delete ir"><?php _e('Remove', $this->plugin_slug); ?></a></li>
										<li><a href="#" class="digi-users-document-button-edit ir"><?php _e('Edit', $this->plugin_slug); ?></a></li>
									</ul>
								</div>
								<?php $icon = get_attachment_icon_src($digi_users_document_stored_meta['digi_users_document_file'][0] );?>
								<img alt="" src="<?php if($digi_users_document_stored_meta['digi_users_document_file'][0]) : echo  $icon[0]; endif; ?>" class="digi-users-document-media-image">
							</div>
							<div class="no-image" <?php if($digi_users_document_stored_meta['digi_users_document_file'][0]) : ?> style="display:none" <?php endif; ?>>
								<p>
									<?php _e('No image selected', $this->plugin_slug); ?>
									<input id="media_attachment_button" type="button" value="<?php _e('Add Image', $this->plugin_slug); ?>" class="button add-media">
								</p>
							</div>
						</div>
					</div>
				</td>
			</tr>
				
		</table>
</div>