//Resetto tutti i valori del media selezionato
function reset_media_attachment_value(){
	jQuery('.has-image').hide();
	jQuery('.digi-users-document-media-image').attr('src','');
	jQuery('.digi-users-document-media-value').val('');
	jQuery('.no-image').fadeIn('slow');
}
function reset_puplish_btn(){
		jQuery('#publishing-action .spinner').hide();
		jQuery('input#publish').removeClass('button-primary-disabled');
		jQuery('input#save-post').removeClass('button-disabled');
		
		jQuery('html, body').animate({
			scrollTop: 0
		},500);
}
jQuery(document).ready(function(){
	//UPLOAD ATTACHMENT
	jQuery('#media_attachment_button').bind('click', function(event) {
	    event.preventDefault();
		jQuery('.digi-users-document-error').remove();
	    //Se il frame dei media già esiste riapro quello.
	//    if (file_frame) {
	//        file_frame.open();
	//        return;
	//    }

	    // Creo il frame dei media.
	    file_frame = wp.media.frames.file_frame = wp.media({
	        title: jQuery(this).data('uploader_title'),
	        button: {
	            text: jQuery(this).data('uploader_button_text'),
	        },
	        multiple: false // Da impostare a true per consentire la selezione multipla dei file
	    });

	    // Quando un immagine è selezionata richiamo le callback.
	    file_frame.on('select', function() {
	        // Abbiamo impostato multiple su false in modo da ottenere solo un immagine dal uploader
	        attachment = file_frame.state().get('selection').first().toJSON();

	        // Fare qualcosa con attachment.id e/o attachment.url qui

	        if(attachment){
				console.log(attachment);
				if(attachment.mime == "image/jpeg" || attachment.mime == "image/png"){
					jQuery('.digi-users-document-media-image').attr('src',attachment.url);
				}else{
					jQuery('.digi-users-document-media-image').attr('src',attachment.icon);
				}
				jQuery('.digi-users-document-media-value').val(attachment.id);
				jQuery('.has-image').fadeIn('slow');
				jQuery('.no-image').hide();
	        }
	    });

	    // Infine, aprire il modale
	    file_frame.open();
	});
	
	
	//AL CLICK DELL'ELIMINA MEDIA
	jQuery('.digi-users-document-button-delete').bind('click',function(e){
		e.preventDefault();
		reset_media_attachment_value();
	});
	//AL CLICK DEL MODIFICA MEDIA
	jQuery('.digi-users-document-button-edit').bind('click',function(e){
		e.preventDefault();
		reset_media_attachment_value();
		jQuery('#media_attachment_button').click();
	});
	
	
	//CONTROLLI PREPOUBBLICAZIONE
	jQuery('#publish').bind('click', function() {
		var stop = false;
		jQuery('.digi-users-document-error').remove();
		
		//Foto
		if(!jQuery('input[name="digi_users_document_file"]').val().length){
			jQuery('.digi-users-document-media-uploader').append('<p class="digi-users-document-error howto">Required Field</p>');
			stop = true;
		}
		
		if(stop){
			setTimeout(function(){reset_puplish_btn();},500);
			return false;
		}
	});
	
	
});