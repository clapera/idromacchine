<?php
/**
 *
 * @package   Digi Users Document
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 *
 * @wordpress-plugin
 * Plugin Name: Digi Users Document
 * Plugin URI:  http://wordpress.org/plugins/digi-users-document/
 * Description: Gestione  document utenti digi_user.
 * Version:     1.0
 * Author:      Diginess s.r.l.
 * Author URI:  http://diginess.com/
 * Text Domain: digi_users_document
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $digi_users_document_options;

// Plugin Root File
if ( ! defined( 'DIGI_USERS_DOCUMENT_PLUGIN_FILE' ) )
	define( 'DIGI_USERS_DOCUMENT_PLUGIN_FILE', __FILE__ );

require_once( plugin_dir_path( __FILE__ ) . 'includes/class-digi-users-document.php' );
#require_once( plugin_dir_path( __FILE__ ) . 'admin/settings/register-settings.php' );
#$digi_users_document_options = digi_users_document_get_settings();

if( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-digi-users-document-admin.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-digi-users-document-meta-box.php' );
} else {
	require_once( plugin_dir_path( __FILE__ ) . 'frontend/class-digi-users-document-frontend.php' );
// 	require_once( plugin_dir_path( __FILE__ ) . 'frontend/class-digi-users-document-shortcode.php' );
}

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'digi-users-document', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'digi-users-document', 'deactivate' ) );

DigiUsersDocument::get_instance();

if( is_admin() ) {
	DigiUsersDocument_Admin::get_instance();
	DigiUsersDocument_Meta_Box::get_instance();
} else {
	DigiUsersDocument_Frontend::get_instance();
// 	DigiUsersDocument_Shortcode::get_instance();
}
	// require_once( plugin_dir_path( __FILE__ ) . 'frontend/digi-users-document-ajax-actions.php' );

	
