<?php
/**
 * Digi Users Document
 *
 * @package   Digi Users Document
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Initial Digi Users Document Frontend class
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 *
 * @since 1.0.0
 */
class DigiUsersDocument_Frontend {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {

		$plugin = DigiUsersDocument::get_instance();
		// Call $plugin_version from public plugin class.
		$this->plugin_version = $plugin->get_plugin_version();
		// Call $plugin_slug from public plugin class.
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load public-facing style sheet and JavaScript.
		// add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
// 		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'dig_user_logged_in', array( $this, 'show_user_file' ) );

	}
	
	//FORMATTAZINE DIMENSIONE FILE
    public function formatBytes($size, $precision = 2) {
        $base = log($size) / log(1024);
        $suffixes = array('B', 'kB', 'MB', 'GB', 'TB');
        $number = round(pow(1024, $base - floor($base)), $precision);
        $unitSize = $suffixes[floor($base)];

        $result = $number . $unitSize;


        if ($unitSize == 'kB'):
            $result = round(round($number) / 1000, 1) . 'MB';
        endif;

        return $result;
    }
	
	public function get_user_document_ids($digi_user_id = false){
		//OTTENGO TUTTI I POST DEI FILE CON I RELATIVI POSTMETA
		$args = array('post_type' => 'digi_users_document', 'posts_per_page' => -1, 'post_status' => 'publish');
		$digi_users_document = new WP_Query($args);
		
		$digi_users_documents = array();
		$file_to_show = array();
		if($digi_users_document->have_posts()){
			$taxonomies = get_object_taxonomies('digi_user', 'objects');
			while($digi_users_document->have_posts()): $digi_users_document->the_post();
				$post_id = get_the_ID();
				$digi_user_stored_meta = get_post_meta( $post_id );
				if($digi_user_stored_meta['digi_users_document_file'][0]){
					$digi_users_documents[$post_id]['digi_users_document_file'] = $digi_user_stored_meta['digi_users_document_file'][0];
					$digi_users_documents[$post_id]['digi_users_document_user'] = unserialize(unserialize($digi_user_stored_meta['digi_users_document_user'][0]));
					$digi_users_documents[$post_id]['digi_users_document_taxonomies'] = unserialize(unserialize($digi_user_stored_meta['digi_users_document_taxonomies'][0]));
					$exlude_digi_user_ids = array();
					
					//Filtro per utente
					if(@!in_array($digi_user_id,$exlude_digi_user_ids) && @in_array($digi_user_id, $digi_users_documents[$post_id]['digi_users_document_user'])){
						$exlude_digi_user_ids[] = $digi_user_id;
						//Questo digi_users_document ha un file da mostrare a questo digi_user
						$file_to_show[] = get_the_ID();
					}
					
					//Filtro tassonomia
					foreach($taxonomies as $taxonomy):
						$terms = get_terms(array($taxonomy->name));
						$digi_user_term_ids = wp_get_post_terms( $digi_user_id, $taxonomy->name, array("fields" => "ids"));
						if(count($terms)):
							foreach($terms as $term):
								foreach($digi_user_term_ids as $digi_user_term_id):
								if(@!in_array($digi_user_id,$exlude_digi_user_ids) && isset($digi_users_documents[$post_id]['digi_users_document_taxonomies'][$taxonomy->name]) && in_array($digi_user_term_id,$digi_users_documents[$post_id]['digi_users_document_taxonomies'][$taxonomy->name])){
									$exlude_digi_user_ids[] = $digi_user_id;
									//Questo digi_users_document ha un file da mostrare a una categoria di questo digi_user
									$file_to_show[] = get_the_ID();
								}
								endforeach;
							endforeach;
						endif;
					endforeach;
				}
			endwhile; 
		}
		
		wp_reset_query();
		
		return $file_to_show;
	}
	
	public function show_user_file($digi_user_logged){
		$digi_user_id = $_COOKIE['digi_user_logged'];
		
		$file_to_show = $this->get_user_document_ids($digi_user_id);
			$html = '';
			if(count($file_to_show)){
				$html .= '<table id="digi-users-document-files">';
				$html .= '<tr>';
				$html .= '<th>'.__('Title', $this->plugin_slug).'</th>';
				$html .= '<th>'.__('Description', $this->plugin_slug).'</th>';
				$html .= '<th>'.__('Type', $this->plugin_slug).'</th>';
				$html .= '<th>'.__('Size', $this->plugin_slug).'</th>';
				$html .= '</tr>';
				foreach($file_to_show as $digi_users_docuemnt_id){
					$digi_users_docuemnt_post = get_post($digi_users_docuemnt_id); 
				$digi_user_stored_meta = get_post_meta( $digi_users_docuemnt_id );
					if($digi_user_stored_meta['digi_users_document_file'][0]){
						$digi_users_document_file_id = $digi_user_stored_meta['digi_users_document_file'][0];
						$digi_users_document_file_url = wp_get_attachment_url( $digi_users_document_file_id );
				$html .= '<tr>';
						$html .= '<td><a target="_blank" href="'.$digi_users_document_file_url.'">'.get_the_title($digi_users_docuemnt_id).'</a></td>';
						$html .= '<td><a target="_blank" href="'.$digi_users_document_file_url.'">'.$digi_users_docuemnt_post->post_content.'</a></td>';
						$icon = get_attachment_icon_src($digi_users_document_file_id);
						$html .= '<td><a target="_blank" href="'.$digi_users_document_file_url.'"><img src="'.$icon[0].'" alt="Icon"/></a></td>';
						$html .= '<td><a target="_blank" href="'.$digi_users_document_file_url.'">'. $this->formatBytes(strlen(file_get_contents($digi_users_document_file_url)), 1).'</a></td>';
				$html .= '</tr>';
					}
				}
				$html .= '</table>';
			}else{
				$html .= '<p>'.__('No Documents Found', $this->plugin_slug).'</p>';
			}
			
			$html .= '<style type="text/css">table#digi-users-document-files{width:100%;border-bottom: 1px solid #DDDDDD;}table#digi-users-document-files tr:hover td{background-color:#1987D3!important}table#digi-users-document-files tr:hover td a{color:#FFFFFF}table#digi-users-document-files tr th,table#digi-users-document-files tr td{border-top: 1px solid #DDDDDD;padding: 15px 5px;text-align: center;vertical-align: middle;} table#digi-users-document-files a{text-decoration:none}table#digi-users-document-files tr:nth-child(even) td{background: #FFFFFF}table#digi-users-document-files tr:nth-child(odd) td{background: #E5E5E5}</style>';
			
			echo $html;
		
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @return   object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		
		$digi_users_document_directory = plugins_url( '/assets/css/', __FILE__ );
		wp_register_style( $this->plugin_slug . '-style', trailingslashit( $digi_users_document_directory ) . 'digi_users_document.css', array(), $this->player_version );
		wp_enqueue_style( $this->plugin_slug . '-style' );
		
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		
		$digi_users_document_directory = plugins_url( '/assets/js/', __FILE__  );
		wp_register_script( $this->plugin_slug . '-script', trailingslashit( $digi_users_document_directory ) . 'digi_users_document.js', array( 'jquery' ), $this->plugin_version, false );
		wp_enqueue_script( $this->plugin_slug . '-script' );
		wp_localize_script( $this->plugin_slug . '-script', 'mainVars',
		array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'permalink' => get_permalink(),
			'messages' => array()
			)
		);
	}


}
