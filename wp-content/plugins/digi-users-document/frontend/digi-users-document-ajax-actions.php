<?php
session_start();


//Ajax
add_action( 'wp_ajax_digi-user-exist', 'digi_user_exist' );
add_action( 'wp_ajax_nopriv_digi-user-exist', 'digi_user_exist' );

function digi_user_exist(){
	$plugin = DigiUsers::get_instance();
	$result = false;
	$email = $_POST['email'];
	$passowrd = md5($_POST['password']);
	if($email && $passowrd){
		global $wpdb;
		$query = "SELECT post_id FROM ".$wpdb->postmeta." WHERE (meta_value = '".$email."' AND meta_key = 'digi-user-email') AND post_id IN (SELECT post_id FROM ".$wpdb->postmeta." WHERE meta_value = '".$passowrd."' AND meta_key = 'digi-user-password')";
		$exists = $wpdb->get_var($query);
	
		if(isset($exists)){
			$result = array('digi_user_id' => $exists);
			$_SESSION['digi_user_id'] = $exists;
		}else{
			$result = array('error' => __('Authentication Failed',$plugin->get_plugin_slug()));
		}
	}else{
			$result = array('error' =>  __('Error Send Data',$plugin->get_plugin_slug()));
	}
	
	header('Content-Type: text/json; charset=UTF-8;');
	echo json_encode($result);
	exit;
}


add_action( 'wp_ajax_digi-user-logout', 'digi_user_logout' );
add_action( 'wp_ajax_nopriv_digi-user-logout', 'digi_user_logout' );

function digi_user_logout(){
	setcookie("digi_user_logged", false, time()+3600, "/");  /* clear */
	unset($_COOKIE['digi_user_logged']);
	
	exit;
}


add_action( 'wp_head', 'logged_user_info' );
function logged_user_info(){

	global $_COOKIE;
	$digi_user_id = $_SESSION['digi_user_id'];
	
	if($digi_user_id){
		
		
		setcookie("digi_user_logged", $digi_user_id, time()+3600, "/");  /* expire in 1 hour */
		unset($_SESSION['digi_user_id']);
		
		wp_redirect(get_permalink());
		exit;
	}
	
}

//Ajax
add_action( 'wp_ajax_digi-forgot-password', 'digi_forgot_password' );
add_action( 'wp_ajax_nopriv_digi-forgot-password', 'digi_forgot_password' );

function digi_forgot_password(){
	$plugin = DigiUsers::get_instance();
	$result = false;
	$email = $_POST['email'];
	$redirect_url = $_POST['permalink'];
	if($email){
		global $wpdb;
		$query = "SELECT post_id FROM ".$wpdb->postmeta." WHERE (meta_value = '".$email."' AND meta_key = 'digi-user-email')";
		$exists = $wpdb->get_var($query);
	
		if(isset($exists)){
			$user_mail = get_post_meta($exists,'digi-user-email',true);
			if($user_mail){
				if(send_password_reset_link($user_mail,$exists,$redirect_url)){
					$result = array('success' =>  __('Follow the instructions that we have forwarded via e-mail',$plugin->get_plugin_slug()));
				}else{
					$result = array('error' => __('Error in sending the e-mail',$plugin->get_plugin_slug()));
				}
				
			}else{
				$result = array('error' => __('No e-mail address set for this user',$plugin->get_plugin_slug()));
			}
		}else{
			$result = array('error' => __('Authentication Failed',$plugin->get_plugin_slug()));
		}
	}else{
			$result = array('error' =>  __('Error Send Data',$plugin->get_plugin_slug()));
	}
	
	header('Content-Type: text/json; charset=UTF-8;');
	echo json_encode($result);
	exit;
}

function send_password_reset_link($user_mail=false,$user_id=false, $redirect_url=false){
	if($user_mail && $user_id && $redirect_url){
		$plugin = DigiUsers::get_instance();
		$url = get_bloginfo('url');
		$domain = parse_url($url, PHP_URL_HOST);
		if($domain == '127.0.0.1'){
			$domain = str_replace(" ","_",get_bloginfo('name')).'.com';
		}
        if (!class_exists('phpmailerException')):
            include (ABSPATH . '/wp-includes/class-phpmailer.php');
            include (ABSPATH . '/wp-includes/class-smtp.php');
        endif;
        $mailUser = new PHPMailer();
        $mailUser->ContentType = 'text/html';
        $mailUser->CharSet = 'UTF-8';
        $mailUser->From = 'no-reply@'.$domain;
        $mailUser->FromName = get_bloginfo('name');
        $mailUser->Subject = get_bloginfo('name') . ' | ' . __('Reset Password Link', $plugin->get_plugin_slug() );
        $mailUser->AddAddress($user_mail);

        $message = '<table width="550" border="0" style="margin:auto;background-color: #F0F0F0;">';

        $message .= '<tr>';
        $message .= '<td>';
        $message .= '<div style="text-transform: uppercase;background-color: #0054A2; color: #FFFFFF; font-size: 15px; padding: 25px; text-align: center;">';
        $message .= __('Reset passowrd', $plugin->get_plugin_slug() );
        $message .= '</div>';
        $message .= '</td>';
        $message .= '</tr>';

        $message .= '<tr>';
        $message .= '<td>';
        $message .= '<div style="padding:15px 25px 15px;">';
		
		$message .= __('If you were to ask to reset your password', $plugin->get_plugin_slug() ). ' <a href="'.$redirect_url.'?digiUserResetPsw='.base64_encode($user_id).'">'.__('click here', $plugin->get_plugin_slug() ).'</a> '.__(', otherwise ignore this email', $plugin->get_plugin_slug() ).'<br/><br/>';
		
        $message .= '</div>';
        $message .= '</td>';
        $message .= '</tr>';

        $message .= '<tr>';
        $message .= '<td>';
        $message .= '<div style="padding:15px 25px 15px; font-size:11px; text-align:center;background-color: #D5D5D5;">';
        $message .= __('This is an automatically generated email, please do not reply.', $plugin->get_plugin_slug() );
        $message .= '</div>';
        $message .= '</td>';
        $message .= '</tr>';

        $message .= '</table>';

        $mailUser->Body = $message;
        if ($mailUser->Send()):
			return true;
		else:
			return false;
            error_log('Errore invio email reset passowrd utente');
        endif;
	}else{
		return false;
	}
}

//Ajax
add_action( 'wp_ajax_digi-user-set-new-password', 'digi_user_set_new_password' );
add_action( 'wp_ajax_nopriv_digi-user-set-new-password', 'digi_user_set_new_password' );

function digi_user_set_new_password(){
	$plugin = DigiUsers::get_instance();
	$result = false;
	$user_id = $_POST['user_id'];
	$new_password = $_POST['new_password'];
	if($user_id && $new_password){
		
	
		if(update_post_meta( $user_id, 'digi-user-password', md5($new_password))){
			$result = array('success' =>  __('The password has been successfully updated',$plugin->get_plugin_slug()));
		}else{
			$result = array('error' => __('It was not possible to update the password',$plugin->get_plugin_slug()));
		}
	}else{
			$result = array('error' =>  __('Error Send Data',$plugin->get_plugin_slug()));
	}
	
	header('Content-Type: text/json; charset=UTF-8;');
	echo json_encode($result);
	exit;
}
