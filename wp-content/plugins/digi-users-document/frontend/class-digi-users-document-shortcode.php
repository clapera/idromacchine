<?php
/**
 * Flowplayer 5 for WordPress
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Flowplayer Shortcode Class
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 *
 * @since 1.3.0
 */
class DigiUsers_Shortcode {

	/**
	 * Instance of this class.
	 *
	 * @since    1.3.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Add shortcode
	 *
	 * @since    1.3.0
	 */
	private function __construct() {

		// Register shortcode
		add_shortcode( 'digi_user', array( $this, 'create_digi_user_output' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since    1.3.0
	 *
	 * @return   object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Create Flowplayer Video HTML Output
	 *
	 * Retrieves a media files and settings to display a video.
	 *
	 * @since    1.3.0
	 *
	 * @param    array $atts Shortcode attributes
	 */
	public function create_digi_user_output( $atts ) {

		if ( isset( $atts['element'] ) ) {
			$element = $atts['element'];
		}

		if ( isset( $element ) ) {

			if($element == 'form-authentication'){
				 if(!isset($_COOKIE['digi_user_logged'])){
					 
 					if($_GET['digiUserResetPsw']){ 
						//Reset Passowrd From
						$return = '<form id="digi-user-reset-password-form" role="form" onsubmit="return digi_users_resetPasswordForm()" method="POST">';

						    $return .= '<div class="form-group">';
								$return .= '<label for="digi-user-new-password">'.__('New Passowrd', $this->plugin_slug ).'</label><br/>';
								$return .= '<input data-required="1" class="form-control" id="digi-user-new-password" name="digi-user-new-password" placeholder="'.__('enter new password', $this->plugin_slug ).'" type="text">';
								$return .= '<label for="digi-user-confirm-new-password">'.__('Confirm New Passowrd', $this->plugin_slug ).'</label><br/>';
								$return .= '<input data-required="1" class="form-control" id="digi-user-confirm-new-password" name="digi-user-confirm-new-password" placeholder="'.__('enter new password', $this->plugin_slug ).'" type="text">';
								$return .= '<input data-required="1" class="form-control" id="digi-user-post-id" name="digi-user-user-post-id" value="'.base64_decode($_GET['digiUserResetPsw']).'" type="hidden">';
							$return .= '</div>';
					
							$return .= '<input  class="btn link-blue" type="submit" value="'.__('Reset password', $this->plugin_slug ).'" />';
						$return .= '</form>';
					}elseif($_GET['forgotpassowrd']){
						//Forgot Passowrd From
						$return = '<form id="digi-user-forgot-form" role="form" onsubmit="return digi_users_forgotPasswordForm()" method="POST">';

						    $return .= '<div class="form-group">';
								$return .= '<label for="digi-user-email">'.__('E-mail', $this->plugin_slug ).'</label><br/>';
								$return .= '<input data-required="1" class="form-control" id="digi-user-email" name="digi-user-email" placeholder="'.__('enter e-mail', $this->plugin_slug ).'" type="text">';
							$return .= '</div>';
					
							$return .= '<input  class="btn link-blue" type="submit" value="'.__('Retrieve password', $this->plugin_slug ).'" />';
						$return .= '</form>';
					}else{
						//Authentication From
						$return = '<form id="digi-user-authentication-form" role="form" onsubmit="return digi_users_authenticationForm()" method="POST">';

						    $return .= '<div class="form-group">';
								$return .= '<label for="digi-user-email">'.__('E-mail', $this->plugin_slug ).'</label><br/>';
								$return .= '<input data-required="1" class="form-control" id="digi-user-email" name="digi-user-email" placeholder="'.__('enter e-mail', $this->plugin_slug ).'" type="text">';
							$return .= '</div>';
					
						    $return .= '<div class="form-group">';
								$return .= '<label for="digi-user-password">'.__('Password', $this->plugin_slug ).'</label><br/>';
								$return .= '<input data-required="1" class="form-control" id="digi-user-password" name="digi-user-password" placeholder="'.__('enter password', $this->plugin_slug ).'" type="password">';
							$return .= '</div>';
					
					
						    $return .= '<div class="form-group">';
							$return .= '<a href="'.get_permalink('url').'?forgotpassowrd=1">'.__('Forgot password', $this->plugin_slug ).'</a>';
							$return .= '</div>';
					
							$return .= '<input  class="btn link-blue" type="submit" value="'.__('Login', $this->plugin_slug ).'" />';
						$return .= '</form>';
					}
					
				}else{
					$return = '<div class="digi-user-msg-alert">'.__('You are already logged in', $this->plugin_slug ).'</div>';
				}
			}
		}

		return $return;

	}

}
