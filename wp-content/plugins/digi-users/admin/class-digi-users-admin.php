<?php
/**
 * Digi Users
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Initial Digi Users Admin class
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 */
class DigiUsers_Admin {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Slug of the plugin screen.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_settings_screen_hook_suffix = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {

		$plugin = DigiUsers::get_instance();
		// Call $plugin_version from public plugin class.
		$this->plugin_version = $plugin->get_plugin_version();
		// Call $plugin_slug from public plugin class.
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Add the options page and menu item.
		// add_action( 'admin_menu', array( $this, 'add_plugin_admin_menu' ) );

		// Load admin style sheet and JavaScript.
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_styles' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

		// Add action & meta links
		$plugin_basename = plugin_basename( plugin_dir_path( DIGI_USERS_PLUGIN_FILE ) . 'digi-users.php' );
		//add_filter( 'plugin_action_links_' . $plugin_basename, array( $this, 'add_action_links' ) );

		// Edit update messages
		add_filter( 'post_updated_messages', array( $this, 'set_messages' ), 10, 2 );

		// Add column and rows
		add_filter( 'manage_edit-digi_user_sortable_columns', array( $this, 'add_sortable_column'), 5, 2 );
		add_filter( 'manage_edit-digi_user_columns', array( $this, 'add_column'), 5, 2 );
		add_action( 'manage_digi_user_posts_custom_column', array( $this, 'add_column_row'), 5, 2 );
		add_filter( 'request', array( $this, 'column_orderby'), 5, 2 );
		//Add Categories Filter Select
		add_action('restrict_manage_posts',array( $this, 'restrict_digi_user_type_filter'), 5, 2);
		
		//Add Categories Filter Select Query
		add_filter('parse_query',array( $this, 'query_digi_user_type_filter'), 5, 2);
		

	}
	public function restrict_digi_user_type_filter() {
		$taxonomy = 'digi_user_type';
		$post_type = 'digi_user';
		if(count(get_terms(array($taxonomy)))){
		    global $typenow;
		    global $wp_query;
		    if ($typenow == $post_type) {
		        $digi_user_taxonomy = get_taxonomy($taxonomy);
		        wp_dropdown_categories(array(
		            'show_option_all' =>  sprintf(__("Show All %s", $this->plugin_slug), $digi_user_taxonomy->label),
		            'taxonomy'        =>  $taxonomy,
		            'orderby'         =>  'name',
		            'selected'        =>  $wp_query->query['term'],
		            'hierarchical'    =>  true,
		            'depth'           =>  3,
		            'show_count'      =>  true, // Show # listings in parens
		            'hide_empty'      =>  false, // Don't show businesses w/o listings
		        ));
		    }
		}
	}
	public function query_digi_user_type_filter($query) {
		$taxonomy = 'digi_user_type';
		$post_type = 'digi_user';
	    global $pagenow;
	    $qv = &$query->query_vars;
	    if ($pagenow=='edit.php' &&
	            isset($qv['post_type']) && $qv['post_type'] == $post_type &&
	            isset($qv['cat']) && is_numeric($qv['cat'])) {
	        $term = get_term_by('id',$qv['cat'],$taxonomy);
	        $qv['term'] = $term->slug;
			$qv['taxonomy'] = $taxonomy;
			unset($qv['cat']);
	    }
	}
	/**
	 * Return an instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @return   object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Register and enqueue admin-specific style sheet.
	 *
	 * @since    1.0.0
	 *
	 * @return   null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_styles() {

		$screen = get_current_screen();

		wp_enqueue_style( $this->plugin_slug .'-admin-styles', plugins_url( '/assets/css/admin.css', __FILE__ ), $this->plugin_version );


	}

	/**
	 * Register and enqueue admin-specific JavaScript.
	 *
	 * @since    1.0.0
	 *
	 * @return   null    Return early if no settings page is registered.
	 */
	public function enqueue_admin_scripts() {

		$screen = get_current_screen();
		$suffix  = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';

		global $post; 
		
		// Only run on digi_uses new and edit post screens
		if ( $screen->post_type === $this->plugin_slug && $screen->base == 'post' ) {

			wp_enqueue_script( $this->plugin_slug . '-action', plugins_url( '/assets/js/action.js', __FILE__ ), array( 'jquery' ), $this->plugin_version, false );
			wp_localize_script( $this->plugin_slug . '-action', 'mainVars',
				array(
					'post' => $post,
					'messages' => array(
						'duplicateField' => __('Existing value', $this->plugin_slug),
						'savedField' => __('Field saved', $this->plugin_slug),
						'limitCt' => __('is the minimum number of characters required for this field', $this->plugin_slug),
						'noMailSet' => __('Was asked to convey the new authentication data but since there is an email address set this is not possible, you want to proceed anyway with the rescue?', $this->plugin_slug),
						'errorMail' =>__('The email you entered is not valid', $this->plugin_slug),
						'requiredField' =>__('Required field', $this->plugin_slug)
					)
				)
			);
			
		}


	}

	/**
	 * Register the administration menu for this plugin into the WordPress Dashboard menu.
	 *
	 * @since    1.0.0
	 */
	public function add_plugin_admin_menu() {

		$this->plugin_settings_screen_hook_suffix = add_submenu_page(
			'edit.php?post_type=digi_user',
			__( 'Digi Users Settings', $this->plugin_slug ),
			__( 'Settings', $this->plugin_slug ),
			'manage_options',
			$this->plugin_slug . '_settings',
			array( $this, 'display_plugin_admin_page' )
		);

	}

	/**
	 * Render the settings page for this plugin.
	 *
	 * @since    1.0.0
	 */
	public function display_plugin_admin_page() {

		include_once( 'views/display-settings.php' );

	}

	/**
	 * Add settings action link to the plugins page.
	 *
	 * @since    1.0.0
	 */
	public function add_action_links( $links ) {

		return array_merge(
			array(
				'settings' => '<a href="' . admin_url( 'edit.php?post_type=digi_user&page=digi_users_settings' ) . '">' . __( 'Settings', $this->plugin_slug ) . '</a>'
			),
			$links
		);

	}

	/**
	 * Edit custom post type messages.
	 *
	 * @since    1.0.0
	 */
	public function set_messages( $messages ) {

		global $post;

		$messages['digi_user'] = apply_filters( 'digi_user_filter_set_messages', array(

			0  => '', // Unused. Messages start at index 1.
			1  => __( 'User updated.', $this->plugin_slug ),
			2  => __( 'Custom field updated.', $this->plugin_slug ),
			3  => __( 'Custom field deleted.', $this->plugin_slug ),
			4  => __( 'User updated.', $this->plugin_slug ) ,
			5  => isset( $_GET['revision'] ) ? sprintf( __( $singular . ' restored to revision from %s', $this->plugin_slug ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
			6  => __( 'User published.', $this->plugin_slug ) ,
			7  => __( 'User saved.', $this->plugin_slug ) ,
			8  => __( 'User submitted.', $this->plugin_slug ) ,
			9  => sprintf( __( 'User scheduled for: %1$s', $this->plugin_slug ), date_i18n( __( 'M j, Y @ G:i' ), strtotime( $post->post_date ) ) ),
			10 => __( 'User draft updated.', $this->plugin_slug ),

		) );

		return $messages;

	}

	/**
	 * Add a column.
	 *
	 * @since    1.1.0
	 */
	public function add_column( $columns ){

		$columns = array(

			'cb'        => '<input type="checkbox" />',
			'email' => __( 'E-mail', $this->plugin_slug ),
			'category' => __( 'Categories', $this->plugin_slug ),
			'date'      => __( 'Date', $this->plugin_slug )

		);

		return $columns;

	}

	/**
	 * Add row
	 *
	 * @since    1.1.0
	 */
	public function add_column_row( $column, $post_id ){
		
		$digi_user_stored_meta = get_post_meta( $post_id );
		$terms = wp_get_post_terms( $post_id, 'digi_user_type' );
		switch ( $column ) {
			case 'email' :
				 if ( isset ( $digi_user_stored_meta['digi-user-email'] ) ) echo edit_post_link(esc_attr( $digi_user_stored_meta['digi-user-email'][0] ), '<strong>', '</strong>');
				break;
			case 'category' :
				 if ($terms):
					 $count = 0;
					 foreach($terms as $term):
						 echo edit_term_link($term->name,false,false,$term);
						 if(count($terms) > 1 && ++$count < count($terms)):
							 echo ", ";
						 endif;
					 endforeach;
				 else:
					 _e('N/D', $this->plugin_slug );
				 endif;
				break;
		}

	}
	
	/**
	 * Add a sortable column.
	 *
	 * @since    1.1.0
	 */
	public function add_sortable_column( $columns ){

		$columns = array(
			'email' => __( 'Email', $this->plugin_slug ),
			'date'      => __( 'Date', $this->plugin_slug  )

		);

		return $columns;

	}
	
	/**
	 * Add a column orderby.
	 *
	 * @since    1.1.0
	 */
	public function column_orderby( $vars ){
		   
   	    if ( isset( $vars['orderby'] ) && 'email' == $vars['orderby'] ) {
   	           $vars = array_merge( $vars, array(
   	               'meta_key' => 'email',
   	               'orderby' => 'meta_value'
   	           ) );
   	       }
	    
	       return $vars;
		
	}


}
