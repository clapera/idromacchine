<?php
/**
 * Digi Users
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * User Meta box class.
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 */
class DigiUsers_Meta_Box {

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug;

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Return an instance of this class.
	 *
	 * @since     1.0.0
	 *
	 * @return    object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * Initializes the meta boxes
	 *
	 * @since     1.0.0
	 */
	public function __construct() {

		$digi_users = DigiUsers::get_instance();
		$this->plugin_slug = $digi_users->get_plugin_slug();

		// Setup the meta boxes for the user
		add_action( 'add_meta_boxes', array( $this, 'add_digi_user_meta_box' ) );

		// Setup the function responsible for saving
		add_action( 'save_post', array( $this, 'save_digi_user_details' ) );
		// Setup the function responsible for deliting
		add_action( 'delete_post ', array( $this, 'delete_digi_user_details' ) );
		
		//Save Authentication Data
		add_action( 'wp_ajax_is-another-user-mail', array( &$this, 'is_another_user_mail' ) );
		

	}
	
	public function is_another_user_mail(){
		$email = $_POST['email'];
		$post_id = $_POST['post_id'];
			
		global $wpdb;
		$exists = $wpdb->get_var("SELECT post_id FROM ".$wpdb->postmeta." WHERE (meta_value = '".$email."' AND meta_key = 'digi-user-email') AND post_id != ".$post_id);
		
		if(isset($exists)){
			echo 1;
		}else{
			echo 0;
		}
		exit;
	}
	
	function notify_authentication($notify){
		if(is_array($notify)){
			
			$url = get_bloginfo('url');
			$domain = parse_url($url, PHP_URL_HOST);
			if($domain == '127.0.0.1'){
				$domain = str_replace(" ","_",get_bloginfo('name')).'.com';
			}
		
            if (!class_exists('phpmailerException')):
                include (ABSPATH . '/wp-includes/class-phpmailer.php');
                include (ABSPATH . '/wp-includes/class-smtp.php');
            endif;
            $mailUser = new PHPMailer();
            $mailUser->ContentType = 'text/html';
            $mailUser->CharSet = 'UTF-8';
            $mailUser->From = 'no-reply@'.$domain;
            $mailUser->FromName = get_bloginfo('name');
            $mailUser->Subject = get_bloginfo('name') . ' | ' . __('Access data', $this->plugin_slug );
            $mailUser->AddAddress($notify['email']);

            $message = '<table width="550" border="0" style="margin:auto;background-color: #F0F0F0;">';

            $message .= '<tr>';
            $message .= '<td>';
            $message .= '<div style="text-transform: uppercase;background-color: #0054A2; color: #FFFFFF; font-size: 15px; padding: 25px; text-align: center;">';
            $message .= __('Updating access data', $this->plugin_slug );
            $message .= '</div>';
            $message .= '</td>';
            $message .= '</tr>';

            $message .= '<tr>';
            $message .= '<td>';
            $message .= '<div style="padding:15px 25px 15px;">';
			$message .= __('Below you will find the access data to the site', $this->plugin_slug ) . ' <a href="'.get_bloginfo('url').'">'.$domain.'</a> '.__('that have been updated', $this->plugin_slug ) . ':<br/><br/>';
			if($notify['email']){
	            $message .= '<strong>' . __('Username', $this->plugin_slug ) . ': ' . '</strong>' . $notify['email'];
	            $message .= '<br/>';
			}
			if($notify['password']){
	            $message .= '<strong>' . __('Password', $this->plugin_slug ) . ': ' . '</strong>' . $notify['password'];
	            $message .= '<br/>';
			}
            $message .= '</div>';
            $message .= '</td>';
            $message .= '</tr>';

            $message .= '<tr>';
            $message .= '<td>';
            $message .= '<div style="padding:15px 25px 15px; font-size:11px; text-align:center;background-color: #D5D5D5;">';
            $message .= __('This is an automatically generated email, please do not reply.', $this->plugin_slug );
            $message .= '</div>';
            $message .= '</td>';
            $message .= '</tr>';

            $message .= '</table>';

            $mailUser->Body = $message;
            if (!$mailUser->Send()):
                error_log('Errore invio notifica utente');
            endif;
			
		}
		
	}
	
	private function generate_password($pos_id=false){
		
		$password = wp_create_nonce( 'digi_user_generate_psw_'.$pos_id );
		
		return $password;
	}
	
	
	private function save_auto_gen_password($pos_id=false,$notify = false){
		
		if(update_post_meta($pos_id,'digi-user-password',md5($this->generate_password($pos_id)))){
			if($notify){
				$notify = array(
					'email' => get_post_meta($pos_id,'digi-user-email',true),
					'password' => $this->generate_password($pos_id)
				);
				$this->notify_authentication($notify);
			}
			return true;
		}else{
			return false;
		}
		
	}
	
	
	private function save_manual_password($pos_id=false, $password = false,$notify = false){
		
		if(update_post_meta($pos_id,'digi-user-password',md5($password))){
			if($notify){
				$notify = array(
					'email' => get_post_meta($pos_id,'digi-user-email',true),
					'password' => $password
				);
				$this->notify_authentication($notify);
			}
			return true;
		}else{
			return false;
		}
		
	}

	/**
	 * Registers the meta box for the post editor.
	 *
	 * @since      1.0.0
	 */
	public function add_digi_user_meta_box() {

		// add_meta_box(
// 			'digi_user_details',
// 			__( 'User Details', $this->plugin_slug ),
// 			array( $this, 'display_digi_user_meta_box' ),
// 			'digi_user',
// 			'normal',
// 			'high'
// 		);
		
		add_meta_box(
			'digi_user_authentication',
			__( 'User Authentication', $this->plugin_slug ),
			array( $this, 'display_digi_user_authentication_meta_box' ),
			'digi_user',
			'normal',
			'default'
		);

	}

	/**
	 * Displays the meta box 
	 *
	 * @since      1.0.0
	 */
	public function display_digi_user_meta_box( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), 'digi-user-nonce' );
		$digi_user_stored_meta = get_post_meta( $post->ID );

		include_once( plugin_dir_path( __FILE__ ) . 'views/display-digi-user-meta-box.php' );

	}
	
	/**
	 * Displays the meta box 
	 *
	 * @since      1.0.0
	 */
	public function display_digi_user_authentication_meta_box( $post ) {

		wp_nonce_field( plugin_basename( __FILE__ ), 'digi-user-nonce' );
		$digi_user_stored_meta = get_post_meta( $post->ID );

		include_once( plugin_dir_path( __FILE__ ) . 'views/display-digi-user-authentication-meta-box.php' );

	}

	/**
	 * When the post is saved or updated, generates a short URL to the existing post.
	 *
	 * @param    int     $post_id    The ID of the post being save
	 * @since    1.0.0
	 */
	public function save_digi_user_details( $post_id ) {

		if ( $this->user_can_save( $post_id, 'digi-user-nonce' ) ) {

			// // Check, validate and save checkboxes
// 			$checkboxes = array(
// 				'fp5-autoplay',
// 				'fp5-loop',
// 				'fp5-fixed-controls',
// 				'fp5-aspect-ratio',
// 				'fp5-fixed-width',
// 				'fp5-no-background',
// 				'fp5-aside-time',
// 				'fp5-no-hover',
// 				'fp5-no-mute',
// 				'fp5-no-time',
// 				'fp5-no-volume',
// 				'fp5-no-embed',
// 				'fp5-play-button'
// 			);
// 
// 			foreach ( $checkboxes as $checkbox ) {
// 				if( isset( $_POST[ $checkbox ] ) ) {
// 					update_post_meta(
// 						$post_id,
// 						$checkbox,
// 						'true'
// 					);
// 				} else {
// 					update_post_meta(
// 						$post_id,
// 						$checkbox,
// 						''
// 					);
// 				}
// 			}
// 
// 			// Check, validate and save keys
// 			$keys = array(
// 				'fp5-select-skin',
// 				'fp5-preload',
// 				'fp5-coloring',
// 				'fp5-ad-type'
// 			);
// 
// 			foreach ( $keys as $key ) {
// 				if( isset( $_POST[ $key ] ) ) {
// 					update_post_meta(
// 						$post_id,
// 						$key,
// 						sanitize_key( $_POST[ $key ] )
// 					);
// 				}
// 			}
// 
// 			// Check, validate and save urls
// 			$urls = array(
// 				'fp5-splash-image',
// 				'fp5-mp4-video',
// 				'fp5-webm-video',
// 				'fp5-ogg-video',
// 				'fp5-hls-video',
// 				'fp5-vtt-subtitles'
// 			);
// 
// 			foreach ( $urls as $url ) {
// 				if( isset( $_POST[ $url ] ) ) {
// 					update_post_meta(
// 						$post_id,
// 						$url,
// 						esc_url_raw( $_POST[ $url ] )
// 					);
// 				}
// 			}
// 
// 			// Check, validate and save numbers
// 			$numbers = array(
// 				'fp5-max-width',
// 				'fp5-width',
// 				'fp5-height',
// 				'fp5-user-id',
// 				'fp5-video-id',
// 				'fp5-ads-time'
// 			);
// 
// 			foreach ( $numbers as $number ) {
// 				if( isset( $_POST[ $number ] ) ) {
// 					update_post_meta(
// 						$post_id,
// 						$number,
// 						absint( $_POST[ $number ] )
// 					);
// 				}
// 			}

			// Check, validate and save text fields
			$text_fields = array(
				'digi-user-email'
			);

			foreach ( $text_fields as $text_field ) {
				if( isset( $_POST[ $text_field ] ) ) {
					update_post_meta(
						$post_id,
						$text_field,
						sanitize_text_field( $_POST[ $text_field ] )
					);
				}
			}
			
			if($_POST['digi-user-email']){
				global $wpdb;

				$wpdb->update( 
					$wpdb->posts, 
					array( 
						'post_title' => $_POST['digi-user-email']
					), 
					array( 'ID' => $post_id ), 
					array( 
						'%s'
					), 
					array( '%d' ) 
				);
			}
			//Settaggio automatico passowrd
			if($_POST['digi-user-auto-set-psw'] && !$_POST['digi-user-manual-set-psw']){
				$this->save_auto_gen_password($post_id, $_POST['digi-user-notify']);
			}elseif($_POST['digi-user-manual-set-psw']){
				$this->save_manual_password($post_id,$_POST['digi-user-manual-set-psw'], $_POST['digi-user-notify']);
			}
			

		}

	}
	
	public function delete_digi_user_details($post_id){
		if ( $this->user_can_save( $post_id, 'digi-user-nonce' ) ) {
			$text_fields = array(
				'digi-user-email',
				'digi-user-password'
			);

			foreach ( $text_fields as $text_field ) {
				if( isset( $_POST[ $text_field ] ) ) {
					delete_post_meta(
						$post_id,
						$text_field
					);
				}
			}
		}
	}

	/**
	 * Determines whether or not the current user has the ability to save meta data associated with this post.
	 *
	 * @param    int     $post_id    The ID of the post being save
	 * @param    string  $nonce      The nonce identifier associated with the value being saved
	 * @return   bool                Whether or not the user has the ability to save this post.
	 * @since    1.0.0
	 */
	private function user_can_save( $post_id, $nonce ) {

		$is_autosave = wp_is_post_autosave( $post_id );
		$is_revision = wp_is_post_revision( $post_id );
		$is_valid_nonce = ( isset( $_POST[ $nonce ] ) && wp_verify_nonce( $_POST[ $nonce ], plugin_basename( __FILE__ ) ) ) ? true : false;

		// Return true if the user is able to save; otherwise, false.
		return ! ( $is_autosave || $is_revision) && $is_valid_nonce;

	}

}
