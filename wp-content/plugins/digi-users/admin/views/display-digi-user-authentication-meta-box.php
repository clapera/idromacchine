<?php
/**
* Digi Users
*
* @package   Digi Users
* @author    Carlo La Pera <carlo.lapera@diginess.it>
* @license   GPL-2.0+
* @link      http://diginess.com/
* @copyright 2014 Diginess s.r.l.
*/
?>
<div class="digi-user-authentication-meta-box">
	<form action="" method="POST">
		<table>
			
			<!-- EMAIL -->	
			<tr>
				<td>
					<label for="digi-user-email" class="digi-user-row-title"><?php _e( 'E-mail', $this->plugin_slug ); ?><sup>*</sup></label><br>
					<input data-required="1" type="text" name="digi-user-email" id="digi-user-email" value="<?php if(isset ($digi_user_stored_meta['digi-user-email'][0])): echo $digi_user_stored_meta['digi-user-email'][0]; endif; ?>" <?php if(!isset ($digi_user_stored_meta['digi-user-email'][0])): ?>placeholder="<?php _e( 'Insert e-mail', $this->plugin_slug ); ?>"<?php  endif; ?>/>
				</td>
			</tr>
			
			<!-- PASSWORD -->
			<tr>
				<td>
					<hr/>
					<label for="digi-user-manual-set-psw" class="digi-user-row-title"><?php _e( 'Password', $this->plugin_slug ); ?></label><br>
					<input type="text" id="digi-user-manual-set-psw" name="digi-user-manual-set-psw" value="" placeholder="<?php _e( 'enter password', $this->plugin_slug ); ?>" />
			<?php if(isset($digi_user_stored_meta['digi-user-password'][0])): ?>
					<p class="howto"><?php _e('If left blank the password will be reset automatically.', $this->plugin_slug ); ?></p>
					<input type="hidden" name="digi-user-auto-set-psw" value="0" />
					<span style="float:left;" class="spinner"></span>
					<input id="digi-user-reset-psw" class="button button-primary button-large" type="submit" value="<?php _e( 'Reset Passowrd', $this->plugin_slug ); ?>">
					<div style="clear:both"></div>
				<?php else: ?>
					<p class="howto"><?php _e('If left blank the password will be automatically set.', $this->plugin_slug ); ?></p>
					<input type="hidden" name="digi-user-auto-set-psw" value="1" />
			<?php endif; ?>
				</td>
			</tr>
			
			<!-- NOtify -->
			<tr>
				<td>
					<hr/>
					<input type="checkbox" id="digi-user-notify" name="digi-user-notify" checked/>
					<label for="digi-user-notify"><?php _e('Notify the user', $this->plugin_slug ); ?></label>
					<p class="howto"><?php _e('If checked will be sent an email with your login information to the address indicated, in case you are setting or resetting the password.', $this->plugin_slug ); ?></p>
				</td>
			</tr>
				
		</table>
	</form>
</div>