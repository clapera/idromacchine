<?php
/**
* Digi Users
*
* @package   Digi Users
* @author    Carlo La Pera <carlo.lapera@diginess.it>
* @license   GPL-2.0+
* @link      http://diginess.com/
* @copyright 2014 Diginess s.r.l.
*/
?>

<?php
$fields = array(
	array(
		'id' => 'firstname',
		'label' => __( 'First Name', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 1,
		'unique' => 0,
		'email' => 0
	),
	array(
		'id' => 'surname',
		'label' => __( 'Surname', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 1,
		'unique' => 0,
		'email' => 0
	),
	array(
		'id' => 'email',
		'label' => __( 'Email', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 1,
		'unique' => 1,
		'email' => 1
	),
	array(
		'id' => 'company',
		'label' => __( 'Company', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 0,
		'unique' => 0,
		'email' => 0
	),
	array(
		'id' => 'taxcode',
		'label' => __( 'Tax Code', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 0,
		'unique' => 0,
		'email' => 0
	),
	array(
		'id' => 'vat',
		'label' => __( 'VAT', $this->plugin_slug ),
		'type' => 'text',
		'requred' => 0,
		'unique' => 0,
		'email' => 0
	)
)
?>
<div class="digi-user-meta-box">
	<table>
		<tr>
			<?php foreach( $fields as $field ): ?>
				<th><label for="digi-user-<?php echo $field['id']; ?>" class="digi-user-row-title"><?php echo $field['label']; ?></label></th>
			<?php endforeach; ?>
		</tr>
		<tr>

			<?php foreach( $fields as $field ): ?>
			<td>
				<input type="<?php echo $field['type']; ?>" name="digi-user-<?php echo $field['id']; ?>" id="digi-user-<?php echo $field['id']; ?>" value="<?php if ( isset ( $digi_user_stored_meta['digi-user-'. $field['id']] ) ) echo esc_attr( $digi_user_stored_meta['digi-user-'. $field['id']][0] ); ?>" data-required="<?php echo $field['requred']; ?>"  data-unique="<?php echo $field['unique']; ?>" data-email="<?php echo $field['email']; ?>" />
			</td>
			<?php endforeach; ?>
		</tr>
	</table>
</div>