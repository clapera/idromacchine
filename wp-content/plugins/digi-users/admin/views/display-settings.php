<?php
/**
 * Display Settings
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */
?>
<div class="wrap">

	<?php screen_icon(); ?>
	<h2><?php echo esc_html( get_admin_page_title() ); ?></h2>

	<?php settings_errors( 'digi_users-notices' ); ?>

	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">

			<div class="post-body-content fp5-main">
				<form method="post" action="options.php">
					<?php
					// This prints out all hidden setting fields
					settings_fields( 'digi_users_settings_group' );
					do_settings_sections( 'digi_users_settings' );
					submit_button();
					?>
				</form>
			</div>

			<div id="postbox-container-1" class="postbox-container-1 digi_users-sidebar">
				<div id="side-sortables" class="ui-sortable metabox-holder">
					<div id="digi_users_about" class="postbox">
					<h3 class="hndle"><span><?php _e( 'About Digi Users', 'digi_users' ); ?></span></h3>
						<div class="inside">
							<p><a target="_blank" title="#" href="#"><?php _e( 'Link Title', 'digi_users' ); ?></a></p>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>

</div>