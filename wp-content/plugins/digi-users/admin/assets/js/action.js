/************************************* USER DETAIL *************************************/
function digi_user_reset_input_check_status(){
	jQuery(".digi-user-error-field").remove();
	jQuery(".digi-user-success-field").remove();
	jQuery("input.error").removeClass('error');
	jQuery("input.error").removeClass('duplicate-mail');
	jQuery("input.success").removeClass('success');
}
//VERIFICA SE UNA MAIL È VALIDA
function digi_user_isValidEmailAddress(emailAddress) {
	var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
	return pattern.test(emailAddress);
}
//VERIFICO SE L'E-MAIL È GIà IN USO DA UN ALTRO UTENTE
function digi_user_validate_user_mail(){
	digi_user_reset_input_check_status();
	var inputElement = jQuery('#digi-user-email');
	var emailAddress = inputElement.val();
	
	if(!digi_user_isValidEmailAddress(emailAddress)){
		inputElement.addClass('error').after('<p class="digi-user-error-field howto">'+mainVars.messages.errorMail+'</p>');
	}else{
		//Verifico se questa mail è già attribuita ad un altro utente
		jQuery.post( ajaxurl, {
			action: 'is-another-user-mail',
			email : emailAddress,  
			post_id : mainVars.post.ID
		}, 
		function( data ) {
			if(data == 1){
				inputElement.addClass('error duplicate-mail').after('<p class="digi-user-error-field howto">'+mainVars.messages.duplicateField+'</p>');
			}
		});
	}
}
//Verifico che i campi obbligatori siano settati 
function digi_user_validate_required_input(){
	var stop = false;
	jQuery('input[data-required="1"]').each(function(){
		if(!jQuery(this).val().length && !jQuery(this).hasClass('error')){
			jQuery(this).addClass('error').after('<p class="digi-user-error-field howto">'+mainVars.messages.requiredField+'</p>');
			stop =true;
		}else if(jQuery(this).hasClass('error') && jQuery(this).val().length > 0 && !jQuery(this).hasClass('duplicate-mail')){
			jQuery(this).removeClass('error').removeClass('duplicate-mail').next('.digi-user-error-field').remove();
		} 
		
		if(jQuery(this).hasClass('error')){
			stop =true;
		}
	});
	
	if(stop){
		jQuery('#publishing-action .spinner').hide();
		jQuery('input#publish').removeClass('button-primary-disabled');
		jQuery('input#save-post').removeClass('button-disabled');
		return false;
	}else{
		return true;
	}
}
//Reimposto la password
function digi_user_reset_psw(){
	jQuery('#digi-user-reset-psw').addClass('button-primary-disabled');
	jQuery('.digi-user-authentication-meta-box .spinner').fadeIn('slow');
	jQuery('input#publish').removeClass('button-primary-disabled');
	jQuery('input#save-post').removeClass('button-disabled');
	if(!jQuery('input[name="digi-user-manual-set-psw"]').val().length){
		jQuery('input[name="digi-user-auto-set-psw"]').val(1);
	}
	jQuery('#publish').click();
}

jQuery(document).ready(function(){
	//Nascondo il titolo del post
	jQuery('#titlediv').hide();
	
	//Alla modifica degli input
	jQuery('#digi-user-email').bind('change',function(){ 
		digi_user_validate_user_mail();
	});
	//Al click del reset psw
	jQuery('#digi-user-reset-psw').bind('click',function(){ 
		digi_user_reset_psw();
	});
	
	//All'invio della form
	jQuery('form#post').submit(function(){
		return digi_user_validate_required_input();
	});
});
