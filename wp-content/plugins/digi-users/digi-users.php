<?php
/**
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 *
 * @wordpress-plugin
 * Plugin Name: Digi Users
 * Plugin URI:  http://wordpress.org/plugins/digi-users/
 * Description: Gestione utenti e area riservata.
 * Version:     1.0
 * Author:      Diginess s.r.l.
 * Author URI:  http://diginess.com/
 * Text Domain: digi_users
 * Domain Path: /languages
 * License:     GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

global $digi_users_options;

// Plugin Root File
if ( ! defined( 'DIGI_USERS_PLUGIN_FILE' ) )
	define( 'DIGI_USERS_PLUGIN_FILE', __FILE__ );

require_once( plugin_dir_path( __FILE__ ) . 'includes/class-digi-users.php' );
#require_once( plugin_dir_path( __FILE__ ) . 'admin/settings/register-settings.php' );
#$digi_users_options = digi_users_get_settings();

if( is_admin() ) {
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-digi-users-admin.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'admin/class-digi-users-meta-box.php' );
} else {
	require_once( plugin_dir_path( __FILE__ ) . 'frontend/class-digi-users-frontend.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'frontend/class-digi-users-shortcode.php' );
}

// Register hooks that are fired when the plugin is activated, deactivated, and uninstalled, respectively.
register_activation_hook( __FILE__, array( 'digi-users', 'activate' ) );
register_deactivation_hook( __FILE__, array( 'digi-users', 'deactivate' ) );

DigiUsers::get_instance();

if( is_admin() ) {
	DigiUsers_Admin::get_instance();
	DigiUsers_Meta_Box::get_instance();
// 	$flowplayer_drive = new Flowplayer_Drive();
// 	add_action( 'plugins_loaded', array( $flowplayer_drive, 'run' ) );
} else {
	DigiUsers_Frontend::get_instance();
	DigiUsers_Shortcode::get_instance();
}
	require_once( plugin_dir_path( __FILE__ ) . 'frontend/digi-users-ajax-actions.php' );

	
