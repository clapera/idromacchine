function reset_input_check_status(){
	jQuery(".digi-user-error-field").remove();
	jQuery(".digi-user-success-field").remove();
	jQuery("input.error").removeClass('error');
	jQuery("input.error").removeClass('duplicate-mail');
	jQuery("input.success").removeClass('success');
}

//Verifico se i dati forniti corrispondono ad un utente esistente
function digi_user_exists(email,password){
	jQuery.post( mainVars.ajaxurl, {
		action: 'digi-user-exist',
		email : email,  
		password : password
	}, 
	function( data ) {
		if(data.error != undefined){
			jQuery('#digi-user-authentication-form input[type="submit"]').before('<p class="digi-user-error-field howto">'+data.error+'</p>');
		}else{
			window.location.reload();
		}
	});
}

//Effettuo il login dell'utente


//Validazione form login
function digi_users_authenticationForm(){
	
	var stop = false;
	reset_input_check_status();
	
	jQuery('#digi-user-authentication-form input[data-required="1"]').each(function(){
		if(!jQuery(this).val().length){
			jQuery(this).addClass('error').after('<p class="digi-user-error-field howto">'+mainVars.messages.requiredField+'</p>');
			var stop = true;
		}else{
			jQuery(this).removeClass('error').next('.digi-user-error-field').remove();
		}
	});
	
	if(jQuery('#digi-user-email').val().length && jQuery('#digi-user-password').val().length && !stop){
		digi_user_exists(jQuery('#digi-user-email').val(), jQuery('#digi-user-password').val());
	}
	
	return false;
}

function digi_users_forgotPasswordForm(){
	reset_input_check_status();
	var email = jQuery('#digi-user-email').val();
	jQuery.post( mainVars.ajaxurl, {
		action: 'digi-forgot-password',
		email : email,
		permalink: mainVars.permalink
	}, 
	function( data ) {
		if(data.error != undefined){
			jQuery('#digi-user-forgot-form input[type="submit"]').before('<p class="digi-user-error-field howto">'+data.error+'</p>');
		}else{
			jQuery('#digi-user-forgot-form').after('<p class="digi-user-success-field howto">'+data.success+'</p>').remove();
		}
	});
	return false;
}

function digiUsersLogout(){
	jQuery.post( mainVars.ajaxurl, {
		action: 'digi-user-logout'
	},function(){
		window.location.reload();
	});
}

function digi_users_resetPasswordForm(){
	reset_input_check_status();
	var stop = false;
	var new_password = jQuery('#digi-user-new-password').val();
	var confirm_password = jQuery('#digi-user-confirm-new-password').val();
	var user_id = jQuery('#digi-user-post-id').val();
	//Verifico che le password conincidono
	if(new_password == confirm_password){
		jQuery.post( mainVars.ajaxurl, {
			action: 'digi-user-set-new-password',
			user_id : user_id,
			new_password: new_password
		}, 
		function( data ) {
			if(data.error != undefined){
				jQuery('#digi-user-reset-password-form input[type="submit"]').before('<p class="digi-user-error-field howto">'+data.error+'</p>');
			}else{
				jQuery('#digi-user-reset-password-form').after('<p class="digi-user-success-field howto">'+data.success+'</p>').remove();
			}
		});
	}else{
		stop = true;
		jQuery('#digi-user-reset-password-form input[type="submit"]').before('<p class="digi-user-error-field howto">'+mainVars.messages.passwordNotMatch+'</p>');
	}
	return false;
}