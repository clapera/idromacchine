<?php
/**
 * Digi Users
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Initial Digi Users Frontend class
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 *
 * @since 1.0.0
 */
class DigiUsers_Frontend {

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization, filters, and administration functions.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {

		$plugin = DigiUsers::get_instance();
		// Call $plugin_version from public plugin class.
		$this->plugin_version = $plugin->get_plugin_version();
		// Call $plugin_slug from public plugin class.
		$this->plugin_slug = $plugin->get_plugin_slug();

		// Load public-facing style sheet and JavaScript.
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_styles' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );

	}

	/**
	 * Return an instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @return   object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Register and enqueue public-facing style sheet.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		
		$digi_users_directory = plugins_url( '/assets/css/', __FILE__ );
		wp_register_style( $this->plugin_slug . '-style', trailingslashit( $digi_users_directory ) . 'digi_users.css', array(), $this->player_version );
		wp_enqueue_style( $this->plugin_slug . '-style' );
	
	}

	/**
	 * Register and enqueues public-facing JavaScript files.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		
		$digi_users_directory = plugins_url( '/assets/js/', __FILE__  );
		wp_register_script( $this->plugin_slug . '-script', trailingslashit( $digi_users_directory ) . 'digi_users.js', array( 'jquery' ), $this->plugin_version, false );
		wp_enqueue_script( $this->plugin_slug . '-script' );
		wp_localize_script( $this->plugin_slug . '-script', 'mainVars',
		array(
			'ajaxurl' => admin_url( 'admin-ajax.php' ),
			'permalink' => get_permalink(),
			'messages' => array(
							'requiredField' => __('Required field', $this->plugin_slug),
							'passwordNotMatch' => __('Passwords entered do not match', $this->plugin_slug)
						)
			)
		);
	}


}
