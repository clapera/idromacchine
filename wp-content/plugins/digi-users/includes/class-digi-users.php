<?php
/**
 * Digi Users
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Initial Digi Users class
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 *
 * @since 1.0.0
 */
class DigiUsers {

	/**
	 * Plugin version, used for cache-busting of style and script file references.
	 *
	 * @since   1.0.0
	 *
	 * @var     string
	 */
	protected $plugin_version = '1.0';

	/**
	 * Unique identifier for your plugin.
	 *
	 * Use this value (not the variable name) as the text domain when internationalizing strings of text. It should
	 * match the Text Domain file header in the main plugin file.
	 *
	 * @since    1.0.0
	 *
	 * @var      string
	 */
	protected $plugin_slug = 'digi_user';

	/**
	 * Instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @var      object
	 */
	protected static $instance = null;

	/**
	 * Initialize the plugin by setting localization.
	 *
	 * @since    1.0.0
	 */
	private function __construct() {

		// Load plugin text domain
		add_action( 'init', array( $this, 'load_plugin_textdomain' ) );

		// Add custom post type
		add_action( 'init', array( $this, 'add_digi_users' ) );

	}

	/**
	 * Return the plugin version.
	 *
	 * @since    1.0.0
	 *
	 *@return    Plugin version variable.
	 */
	public function get_plugin_version() {
		return $this->plugin_version;
	}

	/**
	 * Return the plugin slug.
	 *
	 * @since    1.0.0
	 *
	 *@return    Plugin slug variable.
	 */
	public function get_plugin_slug() {
		return $this->plugin_slug;
	}

	/**
	 * Return an instance of this class.
	 *
	 * @since    1.0.0
	 *
	 * @return   object    A single instance of this class.
	 */
	public static function get_instance() {

		// If the single instance hasn't been set, set it now.
		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;

	}

	/**
	 * Fired when the plugin is activated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Activate" action, false if WPMU is disabled or plugin is activated on an individual blog.
	 */
	public static function activate( $network_wide ) {
		require_once( plugin_dir_path( __FILE__ ) . 'update.php' );
	}

	/**
	 * Fired when the plugin is deactivated.
	 *
	 * @since    1.0.0
	 *
	 * @param    boolean    $network_wide    True if WPMU superadmin uses "Network Deactivate" action, false if WPMU is disabled or plugin is deactivated on an individual blog.
	 */
	public static function deactivate( $network_wide ) {
		// TODO: Define deactivation functionality here
	}

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		$domain = $this->plugin_slug;
		$locale = apply_filters( 'plugin_locale', get_locale(), $domain );

		load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' );
		load_plugin_textdomain( $domain, FALSE, basename( dirname( DIGI_USERS_PLUGIN_FILE ) ) . '/languages/' );

	}

	/**
	 * Add uses Custom Post Type for Digi Users
	 *
	 * @since    1.0.0
	 */
	public function add_digi_users() {

		$labels = apply_filters( 'digi_users_post_type_labels', array(
			'name'                => _x( 'Users', 'Post Type General Name', $this->plugin_slug ),
			'singular_name'       => _x( 'User', 'Post Type Singular Name', $this->plugin_slug ),
			'menu_name'           => __( 'Users', $this->plugin_slug ),
			'parent_item_colon'   => __( 'Parent User', $this->plugin_slug ),
			'all_items'           => __( 'All Users', $this->plugin_slug ),
			'view_item'           => __( 'View User', $this->plugin_slug ),
			'add_new_item'        => __( 'Add New User', $this->plugin_slug ),
			'add_new'             => __( 'New User', $this->plugin_slug ),
			'edit_item'           => __( 'Edit User', $this->plugin_slug ),
			'update_item'         => __( 'Update User', $this->plugin_slug ),
			'search_items'        => __( 'Search Users', $this->plugin_slug ),
			'not_found'           => __( 'No users found', $this->plugin_slug ),
			'not_found_in_trash'  => __( 'No users found in Trash', $this->plugin_slug ),
		) );

		$supports = apply_filters( 'digi_users_post_type_supports', array(
			'title'
		) );

		$rewrite = apply_filters( 'digi_users_post_type_rewrite', array(
			'slug'                => __( 'user', $this->plugin_slug ),
			'with_front'          => true,
			'pages'               => true,
			'feeds'               => true,
		) );


		$args = apply_filters( 'digi_users_post_type_args', array(
			'label'               => __( 'digi_users', $this->plugin_slug ),
			'description'         => __( 'Digi Users', $this->plugin_slug ),
			'labels'              => $labels,
			'supports'            => $supports,
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => false,
			'show_in_admin_bar'   => true,
			'menu_position'       => 15,
			'menu_icon'           => ( version_compare( $GLOBALS['wp_version'], '3.8-alpha', '>' ) ) ? 'dashicons-groups' : '',
			'can_export'          => true,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => true,
			'rewrite'             => $rewrite,
			'query_var'           => 'users',
			'capability_type'     => 'page',
		) );

		register_post_type( 'digi_user', $args );
		
		// Add new taxonomy, make it hierarchical (like categories)
		$labels = array(
			'name'              => _x( 'Categories', 'taxonomy general name', $this->plugin_slug ),
			'singular_name'     => _x( 'Category', 'taxonomy singular name', $this->plugin_slug ),
			'search_items'      => __( 'Search Categories', $this->plugin_slug ),
			'all_items'         => __( 'All Categories', $this->plugin_slug ),
			'parent_item'       => __( 'Parent Category', $this->plugin_slug ),
			'parent_item_colon' => __( 'Parent Category:', $this->plugin_slug ),
			'edit_item'         => __( 'Edit Category', $this->plugin_slug ),
			'update_item'       => __( 'Update Category', $this->plugin_slug ),
			'add_new_item'      => __( 'Add New Category', $this->plugin_slug ),
			'new_item_name'     => __( 'New Category Name', $this->plugin_slug ),
			'menu_name'         => __( 'Category', $this->plugin_slug ),
		);

		$args = array(
			'hierarchical'      => true,
			'labels'            => $labels,
			'show_ui'           => true,
			'show_admin_column' => true,
			'query_var'         => true,
			'rewrite'           => array( 'slug' => 'digi-user-type' ),
		);

		register_taxonomy( 'digi_user_type', array( 'digi_user' ), $args );

	}

}