<?php
/**
 * Update helper
 *
 * @package   Digi Users
 * @author    Carlo La Pera <carlo.lapera@diginess.it>
 * @license   GPL-2.0+
 * @link      http://diginess.com/
 * @copyright 2014 Diginess s.r.l.
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

$digi_users_options = get_option( 'digi_users_options' );
$digi_users_version = get_option( 'digi_users_version' );

if ( $digi_users_options & ! $digi_users_version ) {

	// 1.0.0 is the first version to use this option so we must add it
	$digi_users_version = '1.0';

	#add_option( 'digi_users_options', $digi_users_version );
	add_option( 'digi_users_version', $digi_users_version );

}