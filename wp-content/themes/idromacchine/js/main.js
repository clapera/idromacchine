//Ottengo i valori get nell'url
var urlParams;
(window.onpopstate = function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    urlParams = {};
    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();

//Verifico se nel get è presente qualche ancora
jQuery(document).ready(function(){
if(urlParams.goto != undefined){
	if(jQuery('.goto-'+urlParams.goto).length){
		var top = jQuery('.goto-'+urlParams.goto).position().top;
		jQuery('html, body').animate({
			scrollTop: top
		},500);
	}
}
});

jQuery(document).ready(function(){
	jQuery('#main-menu li').each(function(index){
	    if(index < jQuery('#main-menu li:last').index()){
	        jQuery(this).append('<span></span>');
	    }
	});
});

/* MAGNIFIC POPUP */

jQuery(document).ready(function() {
	
//Gallery
  jQuery('.popup-gallery').magnificPopup({
    delegate: 'a',
    type: 'image',
    tLoading: 'Caricamento immagine #%curr%...',
    mainClass: 'mfp-img-mobile',
    gallery: {
      enabled: true,
      navigateByImgClick: true,
      preload: [0,1], // Will preload 0 - before current, and 1 after the current image
	  tCounter: '%curr% di %total%'
    },
    image: {
      tError: '<a href="%url%">Non è stato possibile caricare l\'immagine #%curr%</a>.',
      titleSrc: function(item) {
        return item.el.attr('title') + '';
      }
    }
  });

//acf_flowplayer_video 
  jQuery('.acf-flowplayer-video-popup').magnificPopup({
	  type: 'ajax',
	  closeOnContentClick:false,
	  closeOnBgClick: false,
	  closeBtnInside: true,
	  showCloseBtn:true,
	  mainClass: 'conatiner-fluid'
  });

  //work_with_us_form 
    jQuery('.iframe-popup').magnificPopup({
  	  type: 'iframe',
  	  closeOnContentClick:false,
  	  closeOnBgClick: true,
  	  closeBtnInside: true,
  	  showCloseBtn:true,
  	  mainClass: 'form-popup',
	  fixedBgPos: true
    });
  
});
