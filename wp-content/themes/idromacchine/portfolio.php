<?php
/**
* Template Name: Portfolio
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	
<?php if( have_rows('lista_portfolio') ): ?>	
	<div class="bg-white">	
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12">
					<?php the_content(); ?>
				</div>
				<br/><br/><br><br><br>
				
				<div class="portfolio-list">
					<?php while( have_rows('lista_portfolio') ): the_row(); 
 
							// vars
							$logo = get_sub_field('logo');
							$nominativo = get_sub_field('nominativo');
							$link = get_sub_field('link');
 
							?>
							<div class="col-xs-3"><a href="<?php echo $link; ?>" title="<?php echo $nominativo; ?>" target="_blank"><img src="<?php echo $logo; ?>" alt="Logo <?php echo $nominativo; ?>"/></a></div>
					<?php endwhile; ?>
				</div>
				
				<div class=clearfix></div>
				<div class="col-xs-12">
					<p>Possiamo affermare, con orgoglio, di aver realizzato per tale Committenza la maggior parte degli impianti di ultima generazione e di avere in corso numerose collaborazioni per le attività di Manutenzione Generica pluriennale degli Impianti presso i più importanti Stabilimenti in Italia.</p>
				</div>
				
			</div>
		</div>
	</div>
<?php endif; ?>	
	
<?php endwhile; ?>


<?php get_footer(); ?>