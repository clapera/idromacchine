<?php
/**
* The template for displaying all pages
*
* This is the template that displays all pages by default.
* Please note that this is the WordPress construct of pages and that
* other 'pages' on your WordPress site will use a different template.
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	
<div class="box-title-page">
	<div class="container">
		<div class="row">
			<div class="col-xs-12">
				<h1><?php the_title(); ?></h1>
			</div>
		</div>
	</div>
</div>

<div class="bg-white">
	<div class="container page-body">
		<div class="row">
			<?php if(have_rows('page_gallery')): ?>
				<div class="col-xs-12 box-text">
					<?php the_content(); ?>
				</div>
				<div class="col-xs-12">
					<div class="row box-gallery bg-white popup-gallery text-center">
						<?php while(have_rows('page_gallery')): the_row(); ?>
							<?php  $thumbArr = wp_get_attachment_image_src(get_sub_field('image'), 'img_140x110'); ?>
							<?php  $imgArr = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>
							<?php if($thumbArr[0]): ?>
								<a class="box-image text-center" style="display:inline-block" href="<?php echo $imgArr[0]; ?>"><img src="<?php echo $thumbArr[0]; ?>"></a>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
			<?php else: ?>
				<div class="col-xs-12 box-text">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php endwhile; ?>


<?php get_footer(); ?>