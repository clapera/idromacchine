<?php
/**
* Template Name: Form Gravity Popup
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/
?>
<div style="display:none">
<?php get_header(); ?>

<link rel='stylesheet' id='gf-mp-form'  href='<?php bloginfo('template_directory'); ?>/css/gf-mp-form.css' type='text/css' media='all' />
</div>



<?php while ( have_posts() ) : the_post(); ?>
	
	<?php
	if(get_the_ID() == 128):
		$icon = 'user';
	else:
		$icon = 'workshirt';
	endif;
	?>
	<div class="form-title">
		<h1><span class="icon-<?php echo $icon; ?>"></span><?php the_title(); ?></h1>
	</div>
	
	<div class="form-content">
		<?php the_content(); ?>
	</div>
	
	<?php if($_GET["posizione"]): ?>
		<script type="text/javascript">
		jQuery('.posizioni-disponibili select option').each(function() {
		    if (jQuery(this).text() == '<?php echo $_GET["posizione"]; ?>'){
		        jQuery(this).prop('selected', true);
		    }
		});
		</script>
	<?php endif; ?>
	
<?php endwhile; ?>



<div style="display:none">
<?php get_footer(); ?>
</div>