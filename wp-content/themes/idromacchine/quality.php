<?php
/**
* Template Name: Quality
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	
<?php if( have_rows('certifications') ): ?>
	<div class="bg-white">	
		<div class="container">
			<div class="row">
				
				<div class="col-xs-12">
					<?php the_content(); ?>
				</div>
				<br/><br/><br><br><br>
				<div class="clearfix"></div>
				<?php $moreCertification = array(); ?>
				<?php $count = 0; while( have_rows('certifications') ): the_row(); $count++;
					
						// vars
						$titolo = get_sub_field('titolo');
						$logo = get_sub_field('logo');
						$testo = html_entity_decode(get_sub_field('testo'));
						$file = get_sub_field('file');
					if($count <= 2):
 					   	
						?>
				<div class="col-xs-6">
						<div class="col-xs-12"><h2><?php echo $titolo; ?></h2></div>
						
						<div class="col-xs-4 text-center">
							<img src="<?php echo $logo; ?>">
						</div>
						<div class="col-xs-8">
							<?php echo $testo; ?>	
						</div>
						
						<div class="clearfix"></div>
						<?php if($file): ?>
						<a target="_blank" href="<?php echo $file; ?>" class="btn btn-download pull-right"><span class="icon-download"></span>SCARICA IL PDF</a>
						<div class="clearfix"></div>
						<?php endif; ?>
						
				</div>
				<?php else:  
					$moreCertification[$count]['titolo'] = $titolo;
					$moreCertification[$count]['logo'] = $logo;
					$moreCertification[$count]['testo'] = $testo;
					$moreCertification[$count]['file'] = $file;
					endif; ?>
				<?php endwhile; ?>
				
			</div>
		</div>
	</div>

<?php if($moreCertification): ?>
	<div class="bg-grey-light">
		<div class="container page-about-us">
			<div class="row">
				
				<?php foreach($moreCertification as $certification): 
					
							// vars
							$titolo = $certification['titolo'];
							$logo = $certification['logo'];
							$testo = $certification['testo'];
							$file =$certification['file'];
 					   	
							?>
						<div class="col-xs-6">
							
								<div class="col-xs-12"><h2><?php echo $titolo; ?></h2></div>
						
								<div class="col-xs-4 text-center">
									<img src="<?php echo $logo; ?>">
								</div>
								<div class="col-xs-8">
									<?php echo $testo; ?>	
								</div>
						
								<div class="clearfix"></div>
								<?php if($file): ?>
								<a target="_blank" href="<?php echo $file; ?>" class="btn btn-download pull-right"><span class="icon-download"></span>SCARICA IL PDF</a>
								<div class="clearfix"></div>
								<?php endif; ?>
						
						</div>

				<?php endforeach; ?>
				
			</div>
		</div>
	</div>
<?php endif; ?>	


<?php endif; ?>	
	
<?php endwhile; ?>


<?php get_footer(); ?>