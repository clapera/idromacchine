<?php
/**
* Template Name: Work with us
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	
	<div class="image-fluid" style="background-image:url('<?php bloginfo("template_directory"); ?>/images/example/work-with-us-slide.png');"></div>
	
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>
	
	<div class="bg-white">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<?php if( have_rows('personale_posizioni_disponibili') ): ?>
						<p><?php the_content(); ?></p>
					<?php else: ?>
						<p><?php _e('Al momento non ci sono posizioni aperte','wp_bootstrap'); ?></p>
					<?php endif;?>
				</div>
			</div>
		</div>
	</div>
	
<?php if( have_rows('personale_posizioni_disponibili') ): ?>
	<div class="container goto-personale">
		<div class="row bg-grey-light">
			
			<!-- <div class="col-xs-12">
				<h2>Personale</h2>
			</div> -->
				
			<?php while ( have_rows('personale_posizioni_disponibili') ) : the_row(); ?>	
				
			<div class="col-xs-6">
				<div class="tab-work">
					<div class="col-xs-1"><span class="icon-user"></span></div>
					<div class="col-xs-10">
						<h3><?php the_sub_field('posizione'); ?></h3>
						<p><?php the_sub_field('descrizione'); ?></p>
					</div>
					<div class="col-xs-1">
						<a class="iframe-popup" href="<?php echo get_permalink(128); ?>?posizione=<?php echo addslashes(get_sub_field('posizione')); ?>"><span class="icon-play-circle"></span></a>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
			
		  <?php endwhile; ?>

		</div>
	</div>
	
	<br/><br/>
<?php endif; ?>


	
				<?php endwhile; ?>


				<?php get_footer(); ?>