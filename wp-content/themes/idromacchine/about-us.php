<?php
/**
* Template Name: About Us
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

<div class="bg-white">	
	<div class="container page-body">
		<div class="row">
			<?php if(have_rows('page_gallery')): ?>
				<div class="col-xs-6 box-text">
					<?php the_content(); ?>
				</div>
				<div class="col-xs-6">
					<div class="row box-gallery bg-white popup-gallery">
						<?php while(have_rows('page_gallery')): the_row(); ?>
							<?php  $thumbArr = wp_get_attachment_image_src(get_sub_field('image'), 'img_140x110'); ?>
							<?php  $imgArr = wp_get_attachment_image_src(get_sub_field('image'), 'full'); ?>
							<?php if($thumbArr[0]): ?>
								<a class="col-xs-4 box-image" href="<?php echo $imgArr[0]; ?>"><img src="<?php echo $thumbArr[0]; ?>"></a>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				</div>
			<?php else: ?>
				<div class="col-xs-12 box-text">
					<?php the_content(); ?>
				</div>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php if(get_field('about_us_text')): ?>
<div class="bg-grey-light">
		<div class="container page-about-us">
			<div class="row">
				<?php if(get_field('about_us_image')): ?>
					<div class="col-xs-6 box-text">
						<?php the_field('about_us_text'); ?>
					</div>
					<div class="col-xs-6">
						<div class="row text-center box-image">
							<?php if(get_field('about_us_image_link')): ?>
								<a target="_blank" href="<?php the_field('about_us_image_link'); ?>">
							<?php endif; ?>
							<?php  $thumbArr = wp_get_attachment_image_src(get_field('about_us_image'), 'img_445x285'); ?>
							<img style="border: 10px solid rgba(255,255,255,0.5);" src="<?php echo $thumbArr[0]; ?>" alt="Immagine" />
							<?php if(get_field('about_us_image_link')): ?>
								</a>
							<?php endif; ?>
						</div>
					</div>
				<?php endif; ?>
			</div>
	</div>
</div>
<?php endif; ?>

<?php endwhile; ?>


<?php get_footer(); ?>