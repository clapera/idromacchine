<?php
/**
* Template Name: Home
*
* @package WordPress
* @subpackage Wp_Bootstrap
* @since Wp Bootstrap 1.0
*/

// Gets header.php
get_header();
?>

<?php if( have_rows('home_slider_images') ): ?>
<?php
srand((float) microtime() * 10000000);
$bgImagesArr = array();
while( have_rows('home_slider_images') ): the_row();
	$bgImagesArr[] = get_sub_field('image');
endwhile;
$chiavi = array_rand($bgImagesArr, 1);
$bgImgUrl = $bgImagesArr[$chiavi];
?>
<div class="slider-home" style="background-image:url('<?php echo $bgImgUrl; ?>')">
	<div class="container">
		
		<div class="row">
			<div class="col-xs-12">
				<?php if(get_field('home_slider_title') || get_field('home_slider_text') || (get_field('slider_link_label') && get_field('home_slider_link'))): ?>
				<div class="content">
				<?php if(get_field('home_slider_title')): ?>
				<h1><?php the_field('home_slider_title'); ?></h1>
				<?php endif; ?>
				<?php if(get_field('home_slider_text')): ?>
				<p><?php the_field('home_slider_text'); ?></p>
				<?php endif; ?>
				<?php if(get_field('slider_link_label') && get_field('home_slider_link')): ?>
				<a class="link-blue" href="<?php the_field('home_slider_link'); ?>"><?php the_field('slider_link_label'); ?></a>
				<?php endif; ?>
				</div>
				<?php endif; ?>
				
				
				
				
			</div>
		</div>
		<?php  if(get_field('acf_flowplayer_video')): $post_flowpayer = get_field('acf_flowplayer_video'); ?>
			<a href="<?php bloginfo('template_directory'); ?>/popup-acf-flowplayer-video.php?flowplayer_id=<?php echo $post_flowpayer->ID; ?>" class="btn btn-slide acf-flowplayer-video-popup"><span class="icon-play"></span>Play Video</a>
		<?php endif; ?> 
		
	</div>
	
</div>
<?php endif; ?>



<?php if( have_rows('home_box_page') ): ?>
<div class="bg-white">
	<div class="container box-page">
		<div class="row">
			<?php while( have_rows('home_box_page') ): the_row(); ?>
				<?php
				$post_object = get_sub_field('page');
				$post_color = get_sub_field('color');
					if( $post_object ): 
					$post = $post_object;
					setup_postdata( $post ); 
					?>
					<div class="col-xs-3 element">
						<a style="border-top: 10px solid <?php echo $post_color; ?>; border-bottom: 10px solid <?php echo $post_color; ?>; "; href="<?php the_permalink(); ?>">
						<?php  $thumbArr = wp_get_attachment_image_src(get_post_thumbnail_id( get_the_ID() ), 'img_220x155'); ?>
						<?php if($thumbArr[0]): ?>
							<img width="220" height="155" src="<?php echo $thumbArr[0]; ?>" alt="Thumbnail" />
						<?php endif; ?>
							<span style="background-color:<?php echo $post_color; ?>" ><?php the_title(); ?>.</span>
						</a>
					</div>
				<?php wp_reset_postdata(); ?>
				<?php endif; ?>
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php endif; ?>


<?php if( get_field('title_1') && get_field('title_2')): ?>
<div class="bg-white">
	<div class="container box-page-menu">
		<div class="row">
			<!-- Box Menu 1 -->
			<?php
				$color = get_field('color_1');
				$title = get_field('title_1');
				$thumb = get_field('thumbnail_1');
			?>
			<div class="col-xs-6 element">
				<div class="title" style="background-color:<?php echo $color; ?>" ><?php echo $title; ?></div>
				<div class="bg">
				<?php  $thumbArr = wp_get_attachment_image_src($thumb, 'img_455x155'); ?>
				<?php if($thumbArr[0]): ?>
					<img width="455" height="155" src="<?php echo $thumbArr[0]; ?>" alt="Thumbnail" />
				<?php endif; ?>
				</div>
				<?php if( have_rows('list_link_page_1') ): ?>
					<div class="link-page">
						<ul>
						<?php while( have_rows('list_link_page_1') ): the_row(); ?>
							<?php $page =  get_page_by_path( basename( untrailingslashit(get_sub_field('page'))), OBJECT, 'page' ); ?>
							<li style="background-color:<?php echo $color; ?>"><a href="<?php the_sub_field('page'); ?>" title="<?php echo get_the_title($page->ID); ?>"><?php echo get_the_title($page->ID); ?></a></li>
						<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
			<!-- Fine Box Menu 1 -->
			
			<!-- Box Menu 2 -->
			<?php
				$color = get_field('color_2');
				$title = get_field('title_2');
				$thumb = get_field('thumbnail_2');
			?>
			<div class="col-xs-6 element">
				<div class="title" style="background-color:<?php echo $color; ?>" ><?php echo $title; ?></div>
				<div class="bg">
				<?php  $thumbArr = wp_get_attachment_image_src($thumb, 'img_455x155'); ?>
				<?php if($thumbArr[0]): ?>
					<img width="455" height="155" src="<?php echo $thumbArr[0]; ?>" alt="Thumbnail" />
				<?php endif; ?>
				</div>
				<?php if( have_rows('list_link_page_2') ): ?>
					<div class="link-page">
						<ul>
						<?php while( have_rows('list_link_page_2') ): the_row(); ?>
							<?php $page =  get_page_by_path( basename( untrailingslashit(get_sub_field('page'))), OBJECT, 'page' ); ?>
							<li style="background-color:<?php echo $color; ?>"><a href="<?php the_sub_field('page'); ?>" title="<?php echo get_the_title($page->ID); ?>"><?php echo get_the_title($page->ID); ?></a></li>
						<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>
			</div>
			<!-- Fine Box Menu 2 -->
		</div>
	</div>
</div>
<?php endif; ?>


<?php if(get_field('chi_siamo_img') && get_field('chi_siamo_text')): ?>
<div class="bg-grey">
	<div class="container chi-siamo">
		<div class="row">
		
			<div class="col-xs-12">
				<h1 class="section-title">Chi siamo</h1>
			</div>
		

			<div class="col-xs-6">
				<?php  $thumbArr = wp_get_attachment_image_src(get_field('chi_siamo_img'), 'img_460x295'); ?>
				<img src="<?php echo $thumbArr[0]; ?>" />
			</div>
		

			<div class="col-xs-6">
				<div class="content">
					<?php the_field('chi_siamo_text'); ?>
				</div>
			</div>
		
		</div>
	</div>
</div>
<?php endif; ?>

<?php if( have_rows('association_and_link') ): ?>
<div class="bg-white">
	<div class="container association-and-link">
		<div class="row text-center">
		
			<div class="col-xs-12 text-left">
				<h1 class="section-title">Association and link</h1>
			</div>
		<?php while( have_rows('association_and_link') ): the_row(); 
 
				// vars
				$logo = get_sub_field('logo');
				$link = get_sub_field('link');
 
				?>
				
				<div class="col-xs-4 association">
					<a href="<?php echo $link; ?>" target="_blank"><img src="<?php echo $logo; ?>" /></a>
				</div>
			
			<?php endwhile; ?>
		</div>
	</div>
</div>
<?php endif; ?>

<?php get_footer(); ?>