<?php
/**
* Template Name: Area Riservata
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

<div class="bg-white">	
	<div class="container page-body">
		<div class="row">
			<div class="col-sm-12">
				<?php the_content(); ?>
			</div>
			
			<div class="col-sm-12">
				
				
				
			</div>
		</div>
	</div>
</div>

<?php endwhile; ?>


<?php get_footer(); ?>