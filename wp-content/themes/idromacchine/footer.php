<?php 
/**
 * Default Footer
 *
 * @package WordPress
 * @subpackage Wp_Bootstrap
 * @since Wp Bootstrap 1.0
 *
 */
 
// Gets all the scripts included by wordpress, wordpress plugins or functions.php 
// using wp_enqueue_script if it has $in_footer set to true
?>
		<div class="well well-lg">
			<div class="container footer">
			<div class="row">
			<ul class="col-xs-12">
				<li><span class="text">P.Iva 00183550276</span><span class="separator"></span></li>
				<li><span class="text">t. +39.041.538.1488</span><span class="separator"></span></li>
				<li><span class="text">f. +39.041.538.0975</span><span class="separator"></span></li>
				<li><a href="<?php echo get_permalink(43); ?>">note legali</a><span class="separator"></span></li>
				<li><a target="_blank" href="">site map</a><span class="separator"></span></li>
				<li><a href="<?php bloginfo('url'); ?>/wp-admin">admin</a><span class="separator"></span></li>
		  	  	<?php if(isset($_COOKIE['digi_user_logged'])): ?>
					<li><a href="javascript:digiUsersLogout();" title="<?php _e('Logout','wp_bootstrap'); ?>"><?php _e('Logout','wp_bootstrap'); ?></a></li>
		        <?php endif;?>
			</ul>
			</div>
			</div>
			
		</div>
<?php wp_footer(); ?>
</body>
</html>