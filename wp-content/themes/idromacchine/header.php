<?php 
/**
 * Default Header
 *
 * @package WordPress
 * @subpackage Wp_Bootstrap
 * @since Wp Bootstrap 1.0
 *
 */?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php wp_title( '|', true, 'right' ); bloginfo('name'); ?></title>
  <link rel="profile" href="http://gmpg.org/xfn/11">
  <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
  <link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.ico" />
 <link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
  <?php 
  // Fires the 'wp_head' action and gets all the scripts included by wordpress, wordpress plugins or functions.php 
  // using wp_enqueue_script if it has $in_footer set to false (which is the default)
  wp_head(); ?>
  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.js"></script>
    <script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
  <![endif]-->
</head>
<body <?php body_class(); ?>>
  <div class="container header">
	  
	  <div class="row">
		  <div class="col-xs-4">
		  	<a class="logo" href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="Logo"></a>
		  </div>
		  <div class="col-xs-8 ">
		        <?php // Loading WordPress Custom Menu
					  // 		           wp_nav_menu( array(
					  // 		              'container_class' => 'idro-box-nav',
					  // 		              'menu_class'      => 'idro-nav text-right',
					  // 		              'menu_id'         => 'top-menu',
					  // 'menu'			=> 'navigazione-top'
					  // 		          ) );
		        ?>
		  </div>
	  
	  </div>
  </div>
  
  <div class="header-nav">
	  <div class="container">
		  <div class="row">
			  <div class="col-xs-9">
				  <nav class="idro-navigation" role="navigation">
           
					  <!-- Collect the nav links for toggling -->
					  <?php // Loading WordPress Custom Menu
					  wp_nav_menu( array(
						  'container_class' => 'idro-box-nav',
						  'menu_class'      => 'idro-nav text-left',
						  'menu_id'         => 'main-menu',
						  'menu'			  => 'navigazione'	
					  ) );
					  ?>
				  </nav>
			  </div>
			  <div class="col-xs-3">
			  	<a class="link-black" href="<?php echo get_permalink(25); ?>" title="<?php echo get_the_title(25); ?>"><span class="icon-lock"></span><?php echo get_the_title(25); ?></a>
			  </div>
		  </div>
	  </div>
  </div>