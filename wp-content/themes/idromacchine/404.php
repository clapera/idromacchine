<?php
/**
 * The template for displaying 404 pages (Not Found)
 *
 * @package WordPress
 * @subpackage Twenty_Fourteen
 * @since Twenty Fourteen 1.0
 */

get_header(); ?>


	<div class="container">
		<div class="row">

			<div class="col-sm-12 text-center">
				<header class="page-header">
					<h1 class="page-title">404</h1>
				</header>

				<div class="page-content">
					<p>Siamo spiacenti non è stato possibile trovare ciò che cercavi.</p>
				</div><!-- .page-content -->
			</div>

		</div>
	</div>

<?php
get_footer();
