<?php
/**
* Template Name: Contact
*
* @package WordPress
* @subpackage Twenty_Fourteen
* @since Twenty Fourteen 1.0
*/

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>
	<div class="box-title-page">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</div>

<div class="bg-white">	
	<div class="container page-body">
		<div class="row">
			<div class="col-sm-9"><div class="bg-gray"><?php the_content(); ?></div></div>
			<div class="col-sm-3">
				<div class="row box-link">
					<?php if( have_rows('personale_posizioni_disponibili',121) ): ?>
					<div class="col-sx-12">
						<div class="bg-gray">
							<p>Vuoi lavorare con noi?<br> Guarda le posizioni aperte</p>
							<a class="btn btn-personale" href="<?php echo get_permalink(121); ?>/?goto=personale">Clicca qui</a>
						</div>
					</div>
					<?php endif; ?>
					<?php if( have_rows('fornitori_posizioni_disponibili',171) ): ?>
					<div class="col-sx-12">
						<div class="bg-gray">
							<p>Vuoi diventare nostro fornitore?</p>
							<a class="btn btn-fornitore"  href="<?php echo get_permalink(171); ?>/?goto=fornitore">Clicca qui</a>
						</div>
					</div>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php endwhile; ?>


<?php get_footer(); ?>