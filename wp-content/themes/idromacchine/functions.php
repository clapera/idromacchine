<?php 
/**
 * Enqueues scripts and styles for front end.
 *
 * @since Wp Bootstrap 1.0
 *
 * @return void
 */
function wp_bootstrapwp_scripts_styles() {
  // Loads Bootstrap minified JavaScript file.
  wp_enqueue_script('bootstrapjs',  get_stylesheet_directory_uri() . '/js/bootstrap.min.js', array('jquery'),'3.0.0', true );
  // Loads Magnific Popup JavaScript file.
  wp_enqueue_script('magnificpopupjs',  get_stylesheet_directory_uri() . '/js/magnific.popup/jquery.magnific-popup.min.js', array('jquery'),'1.0', true );
  // Loads Main minified JavaScript file.
  wp_enqueue_script('mainjs',  get_stylesheet_directory_uri() . '/js/main.js', array('jquery'),'1.0', true );
  // Loads Bootstrap minified CSS file.
  wp_enqueue_style('bootstrapwp',  get_stylesheet_directory_uri() . '/css/bootstrap.min.css', false ,'3.0.0');
  // Loads Whhg minified CSS file.
  wp_enqueue_style('whhg',  get_stylesheet_directory_uri() . '/css/whhg.css', false ,'1.0');
  // Loads Magnific Popup CSS file.
  wp_enqueue_style('magnificpopup',  get_stylesheet_directory_uri() . '/js/magnific.popup/magnific-popup.css', false ,'1.0');
  // Loads our main stylesheet.
  wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array('bootstrapwp') ,'1.0');
}
add_action('wp_enqueue_scripts', 'wp_bootstrapwp_scripts_styles');

if ( ! function_exists( 'wp_bootstrapwp_theme_setup' ) ):
  function wp_bootstrapwp_theme_setup() {
    // Adds the main menu
    register_nav_menus( array(
      'main-menu' => __( 'Main Menu', 'wp_bootstrapwp' ),
    ) );

		add_theme_support( 'post-thumbnails' );
  }
endif;
add_action( 'after_setup_theme', 'wp_bootstrapwp_theme_setup' );

require_once 'inc/nav.php';
require_once 'inc/custom-post.php';
require_once 'inc/customize.php';

// Autocompile select gravity forms
add_filter('gform_pre_render', 'posizione_personale');
function posizione_personale($form){
	
	$lavora_con_noi_page_id = 121;
    
    foreach($form['fields'] as &$field){
        
        if($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posizione-personale') === false)
            continue;
        
        
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $label = 'Seleziona Posizione';
        $choices = array(array('text' => $label, 'value' => ' '));
        
		
		if( have_rows('personale_posizioni_disponibili',$lavora_con_noi_page_id) ):
 
		 	// loop through the rows of data
		    while ( have_rows('personale_posizioni_disponibili',$lavora_con_noi_page_id) ) : the_row();
 
		        // display a sub field value
		        $choices[] = array('text' => addslashes(get_sub_field('posizione')), 'value' => addslashes(get_sub_field('posizione')));
 
		    endwhile;
 
		endif;
        
        $field['choices'] = $choices;
        
    }
    
    return $form;
}
// Autocompile select gravity forms
add_filter('gform_pre_render', 'posizione_fonitore');
function posizione_fonitore($form){
	
	$fornitori_page_id = 171;
    
    foreach($form['fields'] as &$field){
        
        if($field['type'] != 'select' || strpos($field['cssClass'], 'populate-posizione-fornitori') === false)
            continue;
        
        
        // update 'Select a Post' to whatever you'd like the instructive option to be
        $label = 'Candidatura Generica';
        $choices = array(array('text' => $label, 'value' => $label));
        
		
		if( have_rows('fornitori_posizioni_disponibili',$fornitori_page_id) ):
 
		 	// loop through the rows of data
		    while ( have_rows('fornitori_posizioni_disponibili',$fornitori_page_id) ) : the_row();
 
		        // display a sub field value
		        $choices[] = array('text' => addslashes(get_sub_field('posizione')), 'value' => addslashes(get_sub_field('posizione')));
 
		    endwhile;
 
		endif;
        
        $field['choices'] = $choices;
        
    }
    
    return $form;
}

function hide_update_plugins(){
	echo '<style type="text/css">.update-plugins{display:none!important}</style>';
}
add_action( 'admin_footer', 'hide_update_plugins' );

add_filter('gform_notification_1', 'change_user_notification_attachments', 10, 3);
function change_user_notification_attachments( $notification, $form, $entry ) {

    //There is no concept of user notifications anymore, so we will need to target notifications based on other criteria, such as name
    if($notification["name"] == "Notifica Utente" && get_field('attachment_mail_fornitore','option')){
		$attachment_option = get_field('attachment_mail_fornitore','option');
		$attachment_option_dir = get_attached_file( $attachment_option['id']);
		
		if($attachment_option_dir){
			$upload_root = RGFormsModel::get_upload_root();	
			$attachment = preg_replace('|^(.*?)/gravity_forms/|', $upload_root, $attachment_option_dir);
		
			if($attachment){
				$attachments[] = $attachment;
			}     

	        $notification["attachments"] = $attachments;
		}

    }

    return $notification;
}
