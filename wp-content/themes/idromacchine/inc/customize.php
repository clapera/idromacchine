<?php

/* MODIFICO L'URL DEL LOGO IN LOGIN */

function custom_loginlogo_url() {
    return get_bloginfo('url');
}

add_filter('login_headerurl', 'custom_loginlogo_url');

/* PERSONALIZZO IL PANNELLO DI LOGIN */

function custom_login() {
    ?>
    <style type="text/css">
        /* sfondo body */
        body.login,
		html{
			background-color:#fff;
        }
        /* logo */
        h1 a {
            background-image: url("<?php echo get_bloginfo('template_directory'); ?>/images/logo.png") !important;    
            background-position: center center !important;
            background-size: contain !important;
			height: 70px !importnat;
			width: 228px !important;
			margin: auto !important;
			padding: 0 !important;
        }
        .login #nav a, .login #backtoblog a{
            color: #000000 !important; 
            text-shadow:none;
        }
        ombra link 
        .login #nav, .login #backtoblog {
            text-shadow: none;
        }
        pulsante login 
        input.button-primary, button.button-primary, a.button-primary {
            background: url("<?php #echo get_bloginfo('template_directory'); ?>/images/button-grad.png") repeat-x scroll left top #FF2A00;
            background-color: #6A1A0B;
            border-color:#6A1A0B;
        }
        .login form{
            background-color: #092f4f;
            box-shadow: 0 1px 4px -1px #895931;
            -moz-box-shadow: 0 1px 4px -1px #895931;
            -webkit-box-shadow: 0 1px 4px -1px #895931;
        }
        .login label{
            color:#FFFFFF;
        }

        .login .button-primary{
            background-color: #dddddd!important;
            border: 1px solid #FFFFFF!important;
            color: #555555!important;
            cursor: pointer!important;
            font-size: 12px!important;
            background-image:none!important;
        }
        .login .button-primary:hover{
            /*            background-color: #895931!important;
                        color:#FFFFFF!important;*/
        }
        .login form .input{
            font-size: 15px !important;
            padding:8px;
        }
        .description, .form-wrap p{
            color:#FFFFFF!important;
        }
    </style>
    <?php
}

add_action('login_head', 'custom_login');

/* RIMUOVO LE NOTIFICHE DI AGGIORNAMENTO */

function wp_hide_update() {
    #global $current_user;
    #get_currentuserinfo();

    #if ($current_user->ID != 1) {
		// Hide core update notifications
		add_filter ('pre_site_transient_update_core', '__return_zero');

		// Hide theme update notifications
		remove_action ('load-update-core.php', 'wp_update_themes');
		add_filter ('pre_site_transient_update_themes', '__return_zero');

		// Hide plugin update notifications
		remove_action ('load-update-core.php', 'wp_update_plugins');
		add_filter ('pre_site_transient_update_plugins', '__return_zero');
		#}
}

wp_hide_update();



/* RIMUOVO IL LOGO DAL WPNAVMENU */

function remove_wp_logo($wp_admin_bar) {
    $wp_admin_bar->remove_node('wp-logo');
    $wp_admin_bar->remove_node('search');
    $wp_admin_bar->remove_node('new-content');
    $wp_admin_bar->remove_node('comments');
}

add_action('admin_bar_menu', 'remove_wp_logo', 999);

/* NASCONDO DEI SOTTOMENU */

function remove_submenu_menu() {
    global $menu, $current_user, $submenu;
    #if (!isset($current_user->caps['administrator'])):
		unset($menu[5]); //Articoli
		unset($menu[25]); //Commenti
		unset($menu[75]); //Strumenti
	 #endif;
}

add_action('admin_menu', 'remove_submenu_menu');

/* MODIFICO IL FOOTER DEL PANNELLO */

function remove_footer_admin() {
    echo 'Prodotto da <a target="_blank" href="http://www.diginess.com">Diginess</a></p>';
}

add_filter('admin_footer_text', 'remove_footer_admin');

//SETTO LE DIMENSIONI DELLE THUMBNAIL
function thumbs_setup() {
    add_image_size('img_445x285', 445, 285, true);
    add_image_size('img_460x295', 460, 295, true);
    add_image_size('img_455x155', 455, 155, true);
    add_image_size('img_220x155', 220, 155, true);
    add_image_size('img_140x110', 140, 110, true);
}

add_action('after_setup_theme', 'thumbs_setup');

/* ANALITYCS */
function inc_analytics(){
    if(get_option("blog_public") == 1):
    ?>
     <script type="text/javascript"> 
	 
    </script>    
     <?php
     endif;
}

#add_action('wp_head','inc_analytics');

// tradurre le categorie delle custom taxonomy con qtraslate
function qtranslate_edit_taxonomies() {
    $args = array(
        'public' => true,
        '_builtin' => false
    );
    $output = 'object'; // or objects
    $operator = 'and'; // 'and' or 'or'

    $taxonomies = get_taxonomies($args, $output, $operator);

    if ($taxonomies) {
        foreach ($taxonomies as $taxonomy) {
            add_action($taxonomy->name . '_add_form', 'qtrans_modifyTermFormFor');
            add_action($taxonomy->name . '_edit_form', 'qtrans_modifyTermFormFor');
        }
    }
}

#add_action('admin_init', 'qtranslate_edit_taxonomies');