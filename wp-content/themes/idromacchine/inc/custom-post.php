<?php

#add_action( 'init', 'custom_post_init' );
/**
 * Register a book post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function custom_post_init() {
	$labels = array(
		'name'               => _x( 'Books', 'post type general name', 'wp_bootstrap' ),
		'singular_name'      => _x( 'Book', 'post type singular name', 'wp_bootstrap' ),
		'menu_name'          => _x( 'Books', 'admin menu', 'wp_bootstrap' ),
		'name_admin_bar'     => _x( 'Book', 'add new on admin bar', 'wp_bootstrap' ),
		'add_new'            => _x( 'Add New', 'book', 'wp_bootstrap' ),
		'add_new_item'       => __( 'Add New Book', 'wp_bootstrap' ),
		'new_item'           => __( 'New Book', 'wp_bootstrap' ),
		'edit_item'          => __( 'Edit Book', 'wp_bootstrap' ),
		'view_item'          => __( 'View Book', 'wp_bootstrap' ),
		'all_items'          => __( 'All Books', 'wp_bootstrap' ),
		'search_items'       => __( 'Search Books', 'wp_bootstrap' ),
		'parent_item_colon'  => __( 'Parent Books:', 'wp_bootstrap' ),
		'not_found'          => __( 'No books found.', 'wp_bootstrap' ),
		'not_found_in_trash' => __( 'No books found in Trash.', 'wp_bootstrap' ),
	);

	$args = array(
		'labels'             => $labels,
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'book' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments' )
	);

	register_post_type( 'book', $args );
}

// hook into the init action and call create_book_taxonomies when it fires
#add_action( 'init', 'custom_taxonomies_init', 0 );

// create two taxonomies, genres and writers for the post type "book"
function custom_taxonomies_init() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Genres', 'taxonomy general name', 'wp_bootstrap' ),
		'singular_name'     => _x( 'Genre', 'taxonomy singular name', 'wp_bootstrap' ),
		'search_items'      => __( 'Search Genres', 'wp_bootstrap' ),
		'all_items'         => __( 'All Genres', 'wp_bootstrap' ),
		'parent_item'       => __( 'Parent Genre', 'wp_bootstrap' ),
		'parent_item_colon' => __( 'Parent Genre:', 'wp_bootstrap' ),
		'edit_item'         => __( 'Edit Genre', 'wp_bootstrap' ),
		'update_item'       => __( 'Update Genre', 'wp_bootstrap' ),
		'add_new_item'      => __( 'Add New Genre', 'wp_bootstrap' ),
		'new_item_name'     => __( 'New Genre Name', 'wp_bootstrap' ),
		'menu_name'         => __( 'Genre', 'wp_bootstrap' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'genre' ),
	);

	register_taxonomy( 'genre', array( 'book' ), $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Writers', 'taxonomy general name', 'wp_bootstrap' ),
		'singular_name'              => _x( 'Writer', 'taxonomy singular name', 'wp_bootstrap' ),
		'search_items'               => __( 'Search Writers', 'wp_bootstrap' ),
		'popular_items'              => __( 'Popular Writers', 'wp_bootstrap' ),
		'all_items'                  => __( 'All Writers', 'wp_bootstrap' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Writer', 'wp_bootstrap' ),
		'update_item'                => __( 'Update Writer', 'wp_bootstrap' ),
		'add_new_item'               => __( 'Add New Writer', 'wp_bootstrap' ),
		'new_item_name'              => __( 'New Writer Name', 'wp_bootstrap' ),
		'separate_items_with_commas' => __( 'Separate writers with commas', 'wp_bootstrap' ),
		'add_or_remove_items'        => __( 'Add or remove writers', 'wp_bootstrap' ),
		'choose_from_most_used'      => __( 'Choose from the most used writers', 'wp_bootstrap' ),
		'not_found'                  => __( 'No writers found.', 'wp_bootstrap' ),
		'menu_name'                  => __( 'Writers', 'wp_bootstrap' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'writer' ),
	);

	register_taxonomy( 'writer', 'book', $args );
}