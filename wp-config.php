<?php
/**
 * Il file base di configurazione di WordPress.
 *
 * Questo file definisce le seguenti configurazioni: impostazioni MySQL,
 * Prefisso Tabella, Chiavi Segrete, Lingua di WordPress e ABSPATH.
 * E' possibile trovare ultetriori informazioni visitando la pagina: del
 * Codex {@link http://codex.wordpress.org/Editing_wp-config.php
 * Editing wp-config.php}. E' possibile ottenere le impostazioni per
 * MySQL dal proprio fornitore di hosting.
 *
 * Questo file viene utilizzato, durante l'installazione, dallo script
 * di creazione di wp-config.php. Non � necessario utilizzarlo solo via
 * web,� anche possibile copiare questo file in "wp-config.php" e
 * rimepire i valori corretti.
 *
 * @package WordPress
 */

// ** Impostazioni MySQL - E? possibile ottenere questoe informazioni
// ** dal proprio fornitore di hosting ** //
/** Il nome del database di WordPress */
define('DB_NAME', 'Idromacchine');

/** Nome utente del database MySQL */
define('DB_USER', 'demo');

/** Password del database MySQL */
define('DB_PASSWORD', 'd3m0');

/** Hostname MySQL  */
define('DB_HOST', '127.0.0.1');

/** Charset del Database da utilizare nella creazione delle tabelle. */
define('DB_CHARSET', 'utf8');

/** Il tipo di Collazione del Database. Da non modificare se non si ha
idea di cosa sia. */
define('DB_COLLATE', '');

/**#@+
 * Chiavi Univoche di Autenticazione e di Salatura.
 *
 * Modificarle con frasi univoche differenti!
 * E' possibile generare tali chiavi utilizzando {@link https://api.wordpress.org/secret-key/1.1/salt/ servizio di chiavi-segrete di WordPress.org}
 * E' possibile cambiare queste chiavi in qualsiasi momento, per invalidare tuttii cookie esistenti. Ci� forzer� tutti gli utenti ad effettuare nuovamente il login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!i[j|!N&&fY+~_DQBu7D&I$QeNg$4iJ}!GS`p#bC8c.G/k=-#2]Cw~Hrviw[E,71');
define('SECURE_AUTH_KEY',  'Z$Ce{c=e,]FEuNg%e,jFtWge?yH+HZn3/-D2=zHT@M~s}|2y6-g{pcdw`g$6*~IQ');
define('LOGGED_IN_KEY',    'q&RT(!dlSqeM8::6_E!x/cl[0zoh*cs$puz.}VJ2`ucR8+U{`^d)w[W+`./Dc|*u');
define('NONCE_KEY',        '9XOmJn!]19HJv_<gJ4<WPorY&>XMH40o7H1Jlx`0V[Nl&CEi|tTU@,s0*pa<YRHI');
define('AUTH_SALT',        '+|Sq1d,LK7[nGzSoq|jD,[sc5868!pYKDf0DYx/zBp!fi*0ZHcGVr;74p8=wB4v5');
define('SECURE_AUTH_SALT', '>.[Ra])`9KVM}.gF&bDL;IU0NryS+l7R=le?c]iDkzEyKBD^%^1QM+*oZR=yb+~v');
define('LOGGED_IN_SALT',   '*tc~B_Mnt]5,8-3sblP]!<L?++Mk&YymY^2wR>^E7lsfB&xX(>B$3vr7o&RjG))J');
define('NONCE_SALT',       '|I%[1-75($Y8I@1,|6T!g227yFNCa3]twQ<r*YZgw{l6a,++:7dp6`_LyfbRSI-X');

/**#@-*/

/**
 * Prefisso Tabella del Database WordPress .
 *
 * E' possibile avere installazioni multiple su di un unico database if you give each a unique
 * fornendo a ciascuna installazione un prefisso univoco.
 * Solo numeri, lettere e sottolineatura!
 */
$table_prefix  = 'wp_';

/**
 * Lingua di Localizzazione di WordPress, di base Inglese.
 *
 * Modificare questa voce per localizzare WordPress. Occorre che nella cartella
 * wp-content/languages sia installato un file MO corrispondente alla lingua
 * selezionata. Ad esempio, installare de_DE.mo in to wp-content/languages ed
 * impostare WPLANG a 'de_DE' per abilitare il supporto alla lingua tedesca.
 *
 * Tale valore � gi� impostato per la lingua italiana
 */
define('WPLANG', 'it_IT');

/**
 * Per gli sviluppatori: modalit� di debug di WordPress.
 *
 * Modificare questa voce a TRUE per abilitare la visualizzazione degli avvisi
 * durante lo sviluppo.
 * E' fortemente raccomandato agli svilupaptori di temi e plugin di utilizare
 * WP_DEBUG all'interno dei loro ambienti di sviluppo.
 */
define('WP_DEBUG', false);

/* Finito, interrompere le modifiche! Buon blogging. */

/** Path assoluto alla directory di WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Imposta lle variabili di WordPress ed include i file. */
require_once(ABSPATH . 'wp-settings.php');
